<?php

namespace app\admin\controller;

// +----------------------------------------------------------------------
// | 服务公司管理
// +----------------------------------------------------------------------

class Spec extends BaseController {
    /* -----------------------------------END------------------------------- */
    /*
     * 公司管理
     */

    function index($page = 1, $limit = 30, $lang = '', $name = '', $mobile = '', $create_time = '', $min_create_time = '', $max_create_time = '', $is_pass = '', $status = '') {
        if ($this->requestType === 'post') {
            $condition = [];
            if ($name !== '') {
                $condition['name'] = $name;
            }
            if ($mobile !== '') {
                $condition['mobile'] = $mobile;
            }
            if ($lang !== '') {
                $condition['lang'] = $lang;
            }
            if ($create_time !== '') {
                $condition['create_time'] = $create_time;
            }
            if ($min_create_time !== '') {
                $condition['create_time'] = ['>=', strtotime($min_create_time)];
            }
            if ($max_create_time !== '') {
                $condition['create_time'] = ['<=', strtotime($max_create_time . ' 23:59:59')];
            }
            if ($status !== '') {
                $condition['status'] = $status;
            }
            $res = $this->service->model('Company')->getPageList($page, $limit, $condition, 'id desc', '');
            if ($res === false)
                return json(['code' => 1, 'msg' => 'error:' . $this->error('error:' . $this->service->getError())]);
            return json(['code' => 0, 'count' => $res['total_count'], 'data' => $res['data'], 'msg' => '']);
        }

        $list = [
                ['type' => 'select', 'title' => '语言', 'name' => 'lang', 'option' => [['key' => '全部', 'value' => ''], ['key' => '中文', 'value' => '1'], ['key' => '英文', 'value' => '2']]],
                ['type' => 's_e_date', 'title' => '创建时间', 's_name' => 'min_create_time', 'e_name' => 'max_create_time'],
                ['type' => 'select', 'title' => '发布状态', 'name' => 'status', 'option' => [['key' => '全部', 'value' => ''], ['key' => '可用', 'value' => '1'], ['key' => '禁用', 'value' => '0']]],
        ];
        $this->assign('searchForm', $this->html_template->searchForm($list));
        $this->assign('addButton', $this->html_template->addButton('添加公司', 'admin', 'spec', 'addIndex'));
        $this->assign('batheDeleteButton', $this->html_template->batheDeleteButton('table', 'admin', 'spec', 'deleteIndex'));
        return $this->fetch();
    }

    /* ------------------------------------------------------------------ */
    /*
     * 添加
     */

    function addIndex() {
        if ($this->requestType === 'post') {
            $data = input('post.');
            $data['create_time'] = time();
            $rel=$this->service->model('Company')->add($data,'addindex');
            if ($rel) {
                return $this->success('添加成功！');
            }
            return $this->error($this->service->getError());
        }
        $this->assign('langs', $this->service->model('Lang')->getList(['status' => 1]));
        return $this->fetch();
    }

    /* ------------------------------------------------------------------ */
    /*
     * 
     * 编辑
     */

    function editIndex($id = '') {
        $this->assign('langs', $this->service->model('Lang')->getList(['status' => 1]));
        $data=$this->service->model('Company')->getInfo(['id' => $id]);
        $this->assign('data', $data);
        if ($this->requestType == 'post') {
            $temp = input('post.');
            $temp['edit_time'] = time();
            if($temp['name']==$data['name']){
                unset($temp['name']);
            } else if(empty($temp['name'])){return $this->error('公司名称不能为空');}
            if ($this->service->model('Company')->edit($temp, ['id' => $id],'editindex')) {
                return $this->success('success');
            }
            return $this->error($this->service->getError());
        }
        return $this->fetch();
    }

    /* ------------------------------------------------------------------ */
    /*
     * 删除
     */

    function deleteIndex($id = '') {
        if ($this->service->model('Company')->delete($id)) {
            return $this->success('删除成功！');
        }
        return $this->error('删除失败！');
    }

    /* -----------------------------------END------------------------------- */
    /*
     * 信息管理
     */

    function News($page = 1, $limit = 30, $company_id = '', $lang = '', $cover = '', $create_time = '', $title = '', $min_create_time = '', $max_create_time = '', $is_pass = '', $status = '') {
        if ($this->requestType === 'post') {
            $condition = [];
            if ($lang !== '') {
                $condition['lang'] = $lang;
            }
            if ($company_id !== '') {
                $condition['company_id'] = $company_id;
            }
            if ($cover !== '') {
                $condition['cover'] = $cover;
            }
            if ($create_time !== '') {
                $condition['create_time'] = $create_time;
            }
            if ($title !== '') {
                $condition['title'] = $title;
            }
            if ($min_create_time !== '') {
                $condition['create_time'] = ['>=', strtotime($min_create_time)];
            }
            if ($max_create_time !== '') {
                $condition['create_time'] = ['<=', strtotime($max_create_time . ' 23:59:59')];
            }
            if ($status !== '') {
                $condition['status'] = $status;
            }
            $res = $this->service->model('CompanyNews')->getPageList($page, $limit, $condition, 'id desc', '');
            if ($res === false)
                return json(['code' => 1, 'msg' => 'error:' . $this->error('error:' . $this->service->getError())]);
            return json(['code' => 0, 'count' => $res['total_count'], 'data' => $res['data'], 'msg' => '']);
        }



        $list = [
                ['type' => 'select', 'title' => '语言', 'name' => 'lang', 'option' => [['key' => '全部', 'value' => ''], ['key' => '中文', 'value' => 'zh'], ['key' => '英文', 'value' => 'en']]],
                ['type' => 's_e_date', 'title' => '创建时间', 's_name' => 'min_create_time', 'e_name' => 'max_create_time'],
                ['type' => 'select', 'title' => '发布状态', 'name' => 'status', 'option' => [['key' => '全部', 'value' => ''], ['key' => '已发布', 'value' => '1'], ['key' => '未发布', 'value' => '0']]],
                //['type'=>'switch','title'=>'回收站','name'=>'is_delete']
        ];
        $this->assign('searchForm', $this->html_template->searchForm($list));
        $this->assign('addButton', $this->html_template->addButton('添加信息', 'admin', 'spec', 'addNews'));
        $this->assign('batheDeleteButton', $this->html_template->batheDeleteButton('table', 'admin', 'spec', 'deleteNews'));
        $this->assign('company', $this->service->model('Company')->getList(['status' => 1]));
        return $this->fetch();
    }

    /* ------------------------------------------------------------------ */
    /*
     * 添加
     */

    function addNews() {
        if ($this->requestType == 'post') {
            $data = input('post.');
            $data['create_time'] = time();
            if ($this->service->model('CompanyNews')->add($data,'addnews')) {
                return $this->success('success');
            }
            return $this->error($this->service->getError());
        }
        $this->assign('company', $this->service->model('Company')->getList(['status' => 1]));
        $this->assign('langs', $this->service->model('Lang')->getList(['status' => 1]));
        return $this->fetch();
    }

    /* ------------------------------------------------------------------ */
    /*
     * 
     * 编辑
     */

    function editNews($id = '') {
        $data = $this->service->model('CompanyNews')->getInfo(['id' => $id]);
        $this->assign('data', $data);
        $this->assign('langs', $this->service->model('Lang')->getList(['status' => 1]));
        if ($this->requestType == 'post') {
            $data = input('post.');
            $data['edit_time'] = time();

            $rel = $this->service->model('CompanyNews')->edit($data, ['id' => $id],'addnews');
            if ($rel) {
                return $this->success('success');
            }
            return $this->error($this->service->getError());
        }
        $this->assign('company', $this->service->model('Company')->getInfo(['id' => $data['company_id']]));
        return $this->fetch();
    }

    /* ------------------------------------------------------------------ */
    /*
     * 删除
     */

    function deleteNews($id = '') {
        if ($this->service->model('CompanyNews')->delete($id)) {
            return $this->success('删除成功！');
        }
        return $this->error('删除失败！');
    }

    /* -----------------------------------END------------------------------- */
    /*
     * 用户管理
     */

    function User($page = 1, $limit = 30, $create_time = '', $company_id = '', $username = '', $min_create_time = '', $max_create_time = '', $status = '') {
        if ($this->requestType === 'post') {
            $condition = [];
            if ($company_id !== '') {
                $condition['company_id'] = $company_id;
            }
            if ($username !== '') {
                $condition['username'] = $username;
            }
            if ($create_time !== '') {
                $condition['create_time'] = $create_time;
            }
            if ($min_create_time !== '') {
                $condition['create_time'] = ['>=', strtotime($min_create_time)];
            }
            if ($max_create_time !== '') {
                $condition['create_time'] = ['<=', strtotime($max_create_time . ' 23:59:59')];
            }
            if ($status !== '') {
                $condition['status'] = $status;
            }
            $res = $this->service->model('CompanyUser')->getPageList($page, $limit, $condition, 'id desc', '');
            if ($res === false)
                return json(['code' => 1, 'msg' => 'error:' . $this->error('error:' . $this->service->getError())]);
            return json(['code' => 0, 'count' => $res['total_count'], 'data' => $res['data'], 'msg' => '']);
        }



        $list = [
                ['type' => 'select', 'title' => '语言', 'name' => 'lang', 'option' => [['key' => '全部', 'value' => ''], ['key' => '中文', 'value' => '1'], ['key' => '英文', 'value' => '2']]],
                ['type' => 's_e_date', 'title' => '创建时间', 's_name' => 'min_create_time', 'e_name' => 'max_create_time'],
                ['type' => 'select', 'title' => '发布状态', 'name' => 'status', 'option' => [['key' => '全部', 'value' => ''], ['key' => '可用', 'value' => '1'], ['key' => '禁用', 'value' => '0']]],
        ];
        $this->assign('searchForm', $this->html_template->searchForm($list));
        $this->assign('addButton', $this->html_template->addButton('添加用户', 'admin', 'spec', 'addUser'));
        $this->assign('batheDeleteButton', $this->html_template->batheDeleteButton('table', 'admin', 'spec', 'deleteuser'));
        $this->assign('company', $this->service->model('Company')->getList(['status' => 1]));
        return $this->fetch();
    }

    /* ------------------------------------------------------------------ */
    /*
     * 添加
     */

    function addUser() {
        $this->assign('company', $this->service->model('Company')->getList(['status' => 1]));
        if ($this->requestType == 'post') {
            $temp = input('post.');
            $temp['create_time'] = time();
            $temp['password'] = md5($temp['password']);
            $rel = $this->service->model('CompanyUser')->add($temp,'addcompanyuser');
            if ($rel) {
                return $this->success('success');
            }
            return $this->error($this->service->getError());
        }

        return $this->fetch();
    }

    /* ------------------------------------------------------------------ */
    /*
     * 
     * 编辑
     */

    function editUser($id = '') {
        $this->assign('company', $this->service->model('Company')->getList(['status' => 1]));
        $data=$this->service->model('CompanyUser')->getInfo(['id' => $id]);
        $this->assign('data', $data);
        if ($this->requestType == 'post') {
            $temp = input('post.');
            $temp['update_time'] = time();
            if($temp['username']==$data['username']){unset($temp['username']);}
           // if(empty($temp['username'])){ return $this->error('用户名不能为空');}
            if (isset($temp['password']) && !empty($temp['password'])) {
                $temp['password'] = md5($temp['password']);
            } else {
                unset($temp['password']);
            }

            $rel = $this->service->model('CompanyUser')->edit($temp, ['id' => $id],'editcompanyuser');
            if ($rel) {
                return $this->success('success');
            }
            return $this->error($this->service->getError());
        }
        return $this->fetch();
    }

    /* ------------------------------------------------------------------ */
    /*
     * 删除
     */

    function deleteUser($id = '') {
        if ($this->service->model('CompanyUser')->delete($id)) {
            return $this->success('删除成功！');
        }
        return $this->error('删除失败！');
    }

    /* -----------------------------------END------------------------------- */
    /*
     * 文件管理
     */

    function files($page = 1, $limit = 30, $name = '', $create_time = '', $min_create_time = '', $max_create_time = '', $status = '') {
        if ($this->requestType === 'post') {
            $condition = [];
            if ($name !== '') {
                $condition['name'] = $name;
            }
            if ($create_time !== '') {
                $condition['create_time'] = $create_time;
            }
            if ($min_create_time !== '') {
                $condition['create_time'] = ['>=', strtotime($min_create_time)];
            }
            if ($max_create_time !== '') {
                $condition['create_time'] = ['<=', strtotime($max_create_time . ' 23:59:59')];
            }
            if ($status !== '') {
                $condition['status'] = $status;
            }
            $res = $this->service->model('CompanyNewsFiles')->getPageList($page, $limit, $condition, 'id desc', '');
            if ($res === false)
                return json(['code' => 1, 'msg' => 'error:' . $this->error('error:' . $this->service->getError())]);
            return json(['code' => 0, 'count' => $res['total_count'], 'data' => $res['data'], 'msg' => '']);
        }



        $list = [
                ['type' => 's_e_date', 'title' => '创建时间', 's_name' => 'min_create_time', 'e_name' => 'max_create_time'],
                ['type' => 'select', 'title' => '发布状态', 'name' => 'status', 'option' => [['key' => '全部', 'value' => ''], ['key' => '已公开', 'value' => '1'], ['key' => '未公开', 'value' => '0']]],
        ];
        $this->assign('searchForm', $this->html_template->searchForm($list));
        $this->assign('addButton', $this->html_template->addButton('添加文件', 'admin', 'spec', 'addfiles'));
        $this->assign('batheDeleteButton', $this->html_template->batheDeleteButton('table', 'admin', 'Spec', 'deleteFiles'));
        return $this->fetch();
    }

    /* ------------------------------------------------------------------ */
    /*
     * 添加
     */

    function addFiles() {
        $news = $this->service->model('CompanyNews')->getList(['status' => 1]);
        $this->assign('news', $news);
        if ($this->requestType == 'post') {
            $temp = input('post.');
            $temp['create_time'] = time();
            $temp['files'] = $temp['files']['0'];
            if ($this->service->model('CompanyNewsFiles')->add($temp)) {
                return $this->success('添加成功');
            }
            return $this->error('添加失败');
        }
        return $this->fetch();
    }

    /* ------------------------------------------------------------------ */
    /*
     * 
     * 编辑
     */

    function editFiles($id = '') {
        $news = $this->service->model('CompanyNews')->getList(['status' => 1]);
        $this->assign('news', $news);
        $this->assign('data', $this->service->model('CompanyNewsFiles')->getInfo(['id' => $id]));
        if ($this->requestType == 'post') {
            $data = input('post.');
            $rel = $this->service->model('CompanyNewsFiles')->edit($data, ['id' => $id]);
            if ($rel) {
                return $this->success('修改成功');
            }
            return $this->error('修改失败');
        }
        return $this->fetch();
    }

    /* ------------------------------------------------------------------ */
    /*
     * 删除
     */

    function deleteFiles($id = '') {
        if ($this->service->model('CompanyNewsFiles')->delete($id)) {
            return $this->success('删除成功！');
        }
        return $this->error('删除失败！');
    }

}
