<?php
namespace app\admin\controller;
// +----------------------------------------------------------------------
// | 后台首页
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2017 Even Yin All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Even yin <416467069@qq.com> <13022156261>
// +----------------------------------------------------------------------
use think\Db;
use think\Cache;
class Index extends BaseController
{
  protected function __auth(){}
  /* ------------------------------------------------------------------ */
  /*
  * 后台首页
  */
  function index(){
    $db_version = Db::query('select version()');
    $db_version = $db_version? $db_version[0]['version()']: @mysql_get_server_info();
    $row = Db::query("SHOW TABLE STATUS FROM " . config('database.database') . " LIKE '" . config('database.prefix') ."%'");
    $size = 0;
    foreach ($row as $v) {
      $size = $v['Data_length'] + $v['Index_length'];
    }
    $size = round(($size/1048576),2).'M';
    $this->assign('db_size',$size);
    $this->assign('db_version','mysql_'.$db_version);
    return $this->fetch();
  }
  /* ------------------------------------------------------------------ */
  /*
  * 清除缓存
  */
  function clearCache(){
    if(Cache::clear()){
      return $this->success('缓存清除完成！');
    }
    return $this->error('请稍后再试！');
  }
  /* ------------------------------------------------------------------ */
}
