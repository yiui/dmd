<?php
namespace core\model;
// +----------------------------------------------------------------------
// | 系统
// +----------------------------------------------------------------------
class System extends BaseModel
{
    protected $table      = 'system';
    protected $rule       = [];
    protected $scene_rule = [];
    protected $msg        = [];
    protected $scene      = [];
}
