<?php
namespace core\model;
// +----------------------------------------------------------------------
// | 语言
// +----------------------------------------------------------------------
class Lang extends BaseModel
{
    protected $table      = 'lang';
    protected $rule       = [];
    protected $scene_rule = [
        'name'=>'require|unique:Lang|max:10',
        'title'=>'require|unique:Lang|max:20'
    ];
    protected $msg        = [
        'name.require'=>'标识必须',
        'name.unique'=>'标识必须唯一',
        'name.max'=>'表示不能超过10个字符',
        'title.require'=>'标题必须',
        'title.unique'=>'标题必须唯一',
    ];
    protected $scene      = [
        'addlang'=>['name','title'],
        'editllang'=>['name','title'],
        
    ];
    public function getStatusAttr($value){
        return $value=='1'?'可用':'禁用';
    }
}
