DROP TABLE IF EXISTS `d_company_user`;
CREATE TABLE `d_company_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` char(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='公司账户表';
insert into `d_company_user` VALUES  ( '2','3','danna','e10adc3949ba59abbe56e057f20f883e','1509521671','1509523129','1' ), ( '3','4','nbl','e10adc3949ba59abbe56e057f20f883e','1509524150','1509671138','1' ), ( '4','5','kj','e10adc3949ba59abbe56e057f20f883e','1509524165','1509530775','1' ) ;
