<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:68:"D:\wamp\www\dmd\public/../application/admin\view\spec\editfiles.html";i:1509671299;s:60:"D:\wamp\www\dmd\public/../application/admin\view\layout.html";i:1509011174;}*/ ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width,initial-scale=1, maximum-scale=1, user-scalable=no">
  <title>管理后台|江苏大名大律师事务所</title>
  <link rel="stylesheet" href="__PUBLIC__/layui/css/layui.css">
  <script src="__PUBLIC__/layui/layui.js"></script>
  <style>
    body{background-color: #eee;min-width: 1200px;}
    .container{margin:0 auto;width: 1200px;}
    .head{height: 80px;background-color: #fff;border-bottom: 1px solid #ddd;border-top:5px solid #444;}
    .head .logo{#float:left;background-image: url(http://dmd.qianbeinet.com/dmd_logo.png);background-size: cover;width: 200px;height:40px;margin-top: 20px;float: left;}
    .head .layui-nav{background-color:#fff;border-radius:0;float: right;padding:18px 0;}
    .head .layui-nav .layui-nav-more{display: none}
    .head .layui-nav-child{top:62px;box-shadow:none;}
    .head .layui-nav .layui-nav-item{line-height: 24px;text-align: center;}
    .head .layui-nav .layui-nav-item p{font-size: 12px;}
    .head .layui-nav .layui-nav-item a{color: #7A7A7A;font-size: 13px;padding:10px 36px;}
    .head .layui-nav .layui-nav-item a:hover,.head .layui-nav .layui-this a{color: #fff;background-color: #444;font-size: 13px;}
    .head .layui-nav .layui-this:after, .layui-nav-bar, .layui-nav-tree .layui-nav-itemed:after{height:0;}

    .footer{background-color: #eee;color:rgba(0, 0, 0, 0.5);font:14px Helvetica Neue,Helvetica,PingFang SC,\5FAE\8F6F\96C5\9ED1,Tahoma,Arial,sans-serif;}
    .footer a{color:rgba(255,255,255,.6);line-height: 24px;font-size: 13px;color:rgba(255,255,255,.3);}
    .footer .footer-link-block{min-height: 200px;}
    .footer .footer-link-title{font-size: 16px;color: rgba(255,255,255,.8);margin-bottom: 10px;}

    .layui-btn{background-color: #444;}
    .layui-btn-normal{background-color: #1E9FFF;}
    .layui-btn-danger{background-color: #FF5722;}
    .layui-btn-primary{background-color: #fff;}
    .layui-btn-primary:hover{border-color:#444;}
  </style>
  <style media="screen">
    .teacher{min-height:600px;background-color:#fff;border:1px solid #ddd;}
    .layui-nav-tree .layui-nav-item a:hover{}
      .layui-nav-tree .layui-nav-bar{width: 0}
    .layui-nav-tree .layui-nav-item{line-height: 48px}
    .layui-nav-tree .layui-nav-item a{height: 48px;color: #000;text-align: center;}
    .layui-nav-tree .layui-nav-item a:hover{background-color:#444;}
    .layui-nav-tree.layui-nav{background-color: #fff}
    .layui-nav-tree .layui-nav-child dd.layui-this,
     .layui-nav-tree .layui-nav-child dd.layui-this a,
      .layui-nav-tree .layui-this,
       .layui-nav-tree .layui-this>a,
       .layui-nav-tree .layui-this>a:hover{background-color: #444;}
    .layui-nav-tree .layui-this>a{color: #fff;}

    .teacher .layui-tab-card{border: 0;box-shadow: none;border-radius: 0}
    .teacher .layui-tab{margin: 0}
    .teacher .layui-tab .layui-tab-title{height: 48px;background-color: #fff;}
    .teacher .layui-tab .layui-tab-title li{line-height: 48px;padding:0 65px;}
    .teacher .layui-tab .layui-tab-title li.layui-this a{color: #fff;}
    .teacher .layui-tab .layui-tab-title .layui-this{background-color: #444;color: #fff}
    .teacher .layui-tab .layui-tab-title .layui-this:after{height: 48px;border: 0;}
    .teacher .layui-tab .layui-tab-content{padding: 20px}
    .teacher .layui-tab .layui-tab-title .layui-tab-bar{height: 48px;line-height: 48px;}

    .layui-form-item{margin-bottom: 30px;}
    input:disabled{background-color: #eee}
    .layui-form-select dl dd.layui-this{background-color: #444;}
    .layui-laydate .layui-this{background-color: #444!important;}
    .layui-laypage .layui-laypage-curr .layui-laypage-em{background-color: #444;}
  </style>
  
<style>
body{min-width: inherit;background-color: #fff;overflow-x: hidden;}
.teacher{display: none;}
.layui-layout-admin .layui-body{top:10px;}
form{padding:50px 0;}
.layui-footer{width: 100%;position: fixed;bottom: 0;padding:10px 0px;background-color: #eee;text-align: center;}
input:disabled{background-color: #eee;}
</style>

</head>

<body>
  
  
<form class="layui-form" action="" method="post">
	
	 <div class="layui-form-item">
    <div class="layui-inline">
      <label class="layui-form-label">新闻标题</label>
      <div class="layui-input-inline">
        <select name="news_id" required lay-verify="required" lay-filter="" lay-search='true' >
          <option value="" >搜索新闻</option>
          <?php if(is_array($news) || $news instanceof \think\Collection || $news instanceof \think\Paginator): $i = 0; $__LIST__ = $news;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
          <option value="<?php echo $vo['id']; ?>" <?php if($vo['id'] == $data['news_id']): ?>selected<?php endif; ?>><?php echo $vo['title']; ?></option>
          <?php endforeach; endif; else: echo "" ;endif; ?>
        </select>
      </div>
    </div>
  </div>
  
     <div class="layui-form-item">
    <div class="layui-inline">
      <label class="layui-form-label">附件</label>
      <div class="layui-upload layui-input-inline">
        <button type="button" class="layui-btn layui-btn-primary" id="files">上传文件</button><input class="layui-upload-file" type="file" name="files">
        <div class="layui-upload-list" id="files-list">
         
        
          <div style="width: 68px;position: relative;float:left;margin-right:5px;margin-bottom:5px;">
            <img src="<?php echo $vo['cover']; ?>" alt="" width="68" height="68" style="border:1px solid #eee;">
            <input type="hidden" name="files" value="<?php echo $data['files']; ?>" lay-verify="required" autocomplete="off" class="layui-input">
            <div onclick="((this.parentNode).parentNode).removeChild(this.parentNode);" style="background-color:#000;position:absolute;top:0;right:0;padding:2px;"><i class="layui-icon" style="color:#fff;">&#x1007;</i></div>
          </div>
        
       
        </div>
        <div id="files-err"></div>
      </div>
      <script>
        layui.use(['jquery','upload'],function(){
          var $ = layui.jquery;
          var upload = layui.upload;
          var uploadInst = upload.render({
            elem: '#files',
            url: '<?php echo url('upload/file'); ?>',
            size:30*1024,
            accept:'file',
            before: function(obj){

            },
            done: function(res){
              if(res.code == 1){
                var html = '';
                    html += '<div style="width: 68px;position: relative;float:left;margin-right:5px;margin-bottom:5px;">';
                    html += '<img src="'+res.cover+'" alt="" width="68" height="68" style="border:1px solid #eee;">';
                    html += '<input type="hidden" name="files[]" value="'+res.src+'" >';
                    html += '<div onclick="((this.parentNode).parentNode).removeChild(this.parentNode);" style="background-color:#000;position:absolute;top:0;right:0;padding:2px;"><i class="layui-icon" style="color:#fff;">&#x1007;</i></div>';
                    html += '</div>';
                $('#files-list').append(html);
                return true;
              }
            },
            error: function(){
              var errText = $('#files-err');
              errText.html('<span style="color: #FF5722;">上传失败</span> <a class="layui-btn layui-btn-mini files-reload">重试</a>');
              errText.find('.files-reload').on('click', function(){
              	errText.remove();
                uploadInst.upload();
              });
            }
          });
        });
      </script>
    </div>
  </div>


  
  <div class="layui-form-item">
    <div class="layui-inline">
      <label class="layui-form-label">状态</label>
      <div class="layui-input-inline">
        <select name="status" required lay-verify="required" lay-filter="">
          <option value="1" <?php if($data['status'] == '1'): ?>selected<?php endif; ?>>是</option>
          <option value="0" <?php if($data['status'] == '0'): ?>selected<?php endif; ?>>否</option>
        </select>
      </div>
    </div>
  </div>


  <div class="layui-footer">
    <button class="layui-btn layui-btn-normal layui-btn-small" lay-submit lay-filter="*" type="button">立即提交</button>
    <button class="layui-btn layui-btn-small layui-btn-primary" id="closeFrame" type="button">完成/取消</button>
  </div>
</form>
<script>
  layui.use(['layer','jquery','form','layedit'],function(){
    var $ = layui.jquery;
    var form = layui.form;
    var layedit = layui.layedit;
    layedit.set({
    	uploadImage:{
    		url: '<?php echo url('upload/articleImage'); ?>',type:'post'
    	}
    })
    var editIndex = layedit.build('content');

    form.verify({
      title:function(value){if(value.length < 5){return '标题至少得5个字符啊';}}
      ,content:function(value){layedit.sync(editIndex);}
    });

    //监听提交
    form.on('submit(*)', function(data){
      data.form.submit();
      return true;
    });
    $('#closeFrame').on('click',function(){
      parent.layer.closeAll();
    })
    $('#addData').on('click',function(){
      $('form').submit();
    })
  });
</script>

  <div class="container">
    <div class="layui-row layui-col-space10" style="margin-top:5px;">
      <div class="layui-col-xs2">
        <div class="teacher">
          
        </div>
      </div>
      <div class="layui-col-xs10">
        <div class="teacher">
          
        </div>
      </div>
    </div>
  </div>
  


  <script>
      layui.use(['element'],function(){
        var element = layui.element;
      });
  </script>
  

</body>
</html>
