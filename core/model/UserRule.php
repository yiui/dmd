<?php
namespace core\model;
// +----------------------------------------------------------------------
// | 用户
// +----------------------------------------------------------------------

class UserRule extends BaseModel
{
    protected $table      = 'user_rule';
    protected $rule       = [];
    protected $scene_rule = [
     'name'=>'require|unique:user_rule',
      'title'=>'require|unique:user_rule',
        
    ];
    protected $msg        = [
     'title.require'=>'名称必须存在',
        'title.unique'=>'名称不能重复',
        'name.require'=>'控/方，不能为空',
        'name.uniue'=>'控/方不能重复,'
        
    ];
    protected $scene      = [
       'adduserrule'=>['name','title'],
        'edituserrule'=>['name','title'],
    ];
    

}
