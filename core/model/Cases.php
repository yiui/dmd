<?php
namespace core\model;
// +----------------------------------------------------------------------
// | 案例
// +----------------------------------------------------------------------
class Cases extends BaseModel
{
    protected $table      = 'cases';
    protected $rule       = [];
    protected $scene_rule = [
        'title'=>'require|unique:Cases|max:100',
        'cover'=>'require',
        'description'=>'require',
        'content'=>'require',
    ];
    protected $msg        = [
        'title.require'=>'标题不能为空',
        'title.unique'=>'标题不能重复',
        'title.max'=>'标题不能超过100个字符',
        'cover.require'=>'图片必须',
        'description.require'=>'描述必须',
        'content.require'=>'内容必须',
    ];
    protected $scene      = [
        'addcases'=>['title','cover','description','content'],
        'editcases'=>['title.unique','title.max','cover','description','content'],
    ];
}
