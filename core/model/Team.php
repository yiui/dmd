<?php
namespace core\model;
// +----------------------------------------------------------------------
// | 团队
// +----------------------------------------------------------------------
class Team extends BaseModel
{
    protected $table      = 'team';
    protected $rule       = [];
    protected $scene_rule = [
        'name'=>'require|max:20|unique:Team',
        'sex'=>'require',
        'birth'=>'require',
        'mobile'=>'require',
        'cover'=>'require',
        'description'=>'require',
        'content'=>'require',
    ];
    protected $msg        = [
        'name.require'=>'姓名必须',
        'name.unique'=>'该姓名已存在',
        'name.max'=>'姓名不能超过20个字符',
        'sex.require'=>'性别必须',
        'mobile.require'=>'手机必须',
        'description.require'=>'描述必须',
        'content.require'=>'内容必须'
    ];
    protected $scene      = [
        'addteam'=>['name','sex','mobile','description','content'],
        'editteam'=>['name.require','name.max','sex','mobile','description','content'],
        
    ];
}
