DROP TABLE IF EXISTS `d_team`;
CREATE TABLE `d_team` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL DEFAULT ' ',
  `sex` int(1) DEFAULT '1',
  `mobile` bigint(11) DEFAULT NULL,
  `birth` int(11) DEFAULT NULL,
  `cover` varchar(100) DEFAULT ' ',
  `description` text,
  `content` text,
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `lang` int(1) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `is_public` int(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='团队列表';
insert into `d_team` VALUES  ( '7','11','1','18712345678','2017','\uploads\1\20171104\d8a9151d237affba22788d0b0107e624.jpg','描述','内容','1509758229','1509758229','','0','1' ), ( '8','杰克','0','13212345678','1970','\uploads\1\20171104\436285d50588a83542444cc31040e8d0.jpg','内容','你1111','1509760382','1509760382','','0','1' ), ( '9','222','0','12222222222','1970','\uploads\1\20171104\68ce8be1421aaf3566da706d3d4f540e.jpg','22','22','1509761260','1509761260','','1','1' ) ;
