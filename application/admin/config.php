<?php
return [
  'dispatch_success_tmpl'  => APP_PATH . 'admin' . DS . 'view' . DS . 'success.html',
  'dispatch_error_tmpl'    => APP_PATH . 'admin' . DS . 'view' . DS . 'error.html',
];
