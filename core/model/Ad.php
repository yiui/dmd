<?php
namespace core\model;
// +----------------------------------------------------------------------
// | 广告
// +----------------------------------------------------------------------
class Ad extends BaseModel
{
    protected $table      = 'ad';
    protected $rule       = [];
    protected $scene_rule = [
        'cid'=>'require',
    'name'=>'require|unique:Ad|max:20',
     'cover'=>'require',
      'description'=>'require'
    ];
    protected $msg        = [
        'cid.require'=>'分类必须',
        'name.require'=>'名称必须',
        'name.unique'=>'名称必须唯一',
        'name.max'=>'名称不超过20个字符',
        'cover.require'=>'图片不存在',
        'description.require'=>'描述不能为空'
    ];
    protected $scene      = [
        'addad'=>['cid','name','cover','description'],
        'editad'=>['cid','name.unique','name.max','cover','description'],
        
    ];
    
    protected function getCidAttr($value){
        $rel=db('ad_category')->where(['id'=>$value])->find();
        if($rel){
            return $rel['name'];
        }
        return null;
    }
    protected function getStatusAttr($value){
        return $value=='1'?'可用':'禁用';
    }
    
    
    //模型验证

}
