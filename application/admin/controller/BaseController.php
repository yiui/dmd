<?php
namespace app\admin\controller;
// +----------------------------------------------------------------------
// | 平台基础控制层
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2017 Even Yin All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Even yin <416467069@qq.com> <13022156261>
// +----------------------------------------------------------------------
use think\Controller;
use core\service\BaseService as BaseService;
use app\admin\controller\Html as HtmlTemplate;
class BaseController extends Controller
{
  protected $uid;
  protected $user;
  protected $service;
  protected $system;
  protected $requestType;
  protected $module;
  protected $controller;
  protected $action;
  protected $html_template;
  /* ------------------------------------------------------------------ */
  /*
  * 构造函数
  */
  public function __construct()
  {
    parent::__construct();
    $this->service = new BaseService();
    $this->uid = session('user.id');
    $this->user = session('user');
    $this->system = $this->service->getSystem();
    $this->module = $this->service->getModule();
    $this->controller = $this->service->getController();
    $this->action = $this->service->getAction();
    $this->requestType = $this->service->getRequestType();
    $this->assign('system',$this->system);
    $this->assign('module',$this->module);
    $this->assign('controller',$this->controller);
    $this->assign('action',$this->action);
    $this->__init();
    $this->__auth();
    $this->html_template = new HtmlTemplate();
  }
  /* ------------------------------------------------------------------ */
  /*
  * 初始函数
  */
  protected function __init(){
    if(empty($this->uid)) return $this->redirect(url('user/login'));
    $this->assign('uid',$this->uid);
    $this->assign('user',$this->user);
  }
  /* ------------------------------------------------------------------ */
  /*
  * 权限检测
  */
  protected function __auth(){
    if($this->uid!==1 ){
      echo $this->error('您没有此权限！');exit;
    }
  }
  /* ------------------------------------------------------------------ */
  /*
  * 空页面
  */
  public function _empty(){
    return $this->error('404页面不存在！');
  }
  /* ------------------------------------------------------------------ */
}
