<?php
namespace core\model;
// +----------------------------------------------------------------------
// | 广告分类
// +----------------------------------------------------------------------
class AdCategory extends BaseModel
{
    protected $table      = 'ad_category';
    protected $rule       = [];
    protected $scene_rule = [
        'name'=>'require|unique:AdCategory|max:20',
        'description'=>'require',
    ];
    
    protected $msg        = [
        'name.require'=>'名称必须',
        'name.unique'=>'名称必须唯一',
        'name.max'=>'名称不超过20个字符',
        'description.require'=>'描述必须',
    ];
    protected $scene      = [
        'addadcategory'=>['name','description'],
        'editadcategory'=>['name','description'],
    ];
    protected function getStatusAttr($value){
        return $value=='1'?'可用':'禁用';
    }
}
