<?php
namespace app\admin\controller;
// +----------------------------------------------------------------------
// | 服务管理
// +----------------------------------------------------------------------

class Service extends BaseController
{
  /* -----------------------------------END------------------------------- */
  /*
  * 服务管理
  */
  function index($page=1,$limit=30,$lang='',$title='',$create_time='',$min_create_time='',$max_create_time='',$status=''){
    if($this->requestType === 'post'){
      $condition = [];
      if($lang!==''){$condition['lang']=$lang;}
      if($title!==''){$condition['title']=$title;}
      if($create_time!==''){$condition['create_time']=$create_time;}
      if($min_create_time!==''){$condition['create_time']=['>=',strtotime($min_create_time)];}
      if($max_create_time!==''){$condition['create_time']=['<=',strtotime($max_create_time.' 23:59:59')];}
      if($status!==''){$condition['status']=$status;}
      $res = $this->service->model('Service')->getPageList($page,$limit,$condition,'id desc','');
      if($res === false) return json(['code'=>1,'msg'=>'error:'.$this->error('error:'.$this->service->getError())]);
      return json(['code'=>0,'count'=>$res['total_count'],'data'=>$res['data'],'msg'=>'']);
    }



    $list = [
      ['type'=>'select','title'=>'语言','name'=>'lang','option'=>[['key'=>'全部','value'=>''],['key'=>'中文','value'=>'zh'],['key'=>'英文','value'=>'en']]],
      ['type'=>'s_e_date','title'=>'创建时间','s_name'=>'min_create_time','e_name'=>'max_create_time'],
      ['type'=>'select','title'=>'发布状态','name'=>'status','option'=>[['key'=>'全部','value'=>''],['key'=>'已公开','value'=>'1'],['key'=>'未公开','value'=>'0']]],
    ];
    $this->assign('searchForm',$this->html_template->searchForm($list));
    $this->assign('addButton',$this->html_template->addButton('添加项目','admin','service','addservice'));
    $this->assign('batheDeleteButton',$this->html_template->batheDeleteButton('table','admin','service','deleteservice'));
    return $this->fetch();
  }
  /* ------------------------------------------------------------------ */
  /*
  * 添加
  */
  function addService(){
      $this->assign('langs',$this->service->model('Lang')->getList(['status'=>1]));
      if($this->requestType=='post'){
          $temp=input('post.');
          $temp['create_time']=$temp['update_time']=time();
          $temp['content'] = htmlspecialchars($temp['content']);
           
          $rel=$this->service->model('Service')->add($temp,'addservice');
          if($rel){
              return $this->success('添加成功');
          }
          return $this->error($this->service->getError());
      }
  	return $this->fetch();
  }
  /* ------------------------------------------------------------------ */
  /*
   * 
  * 编辑
  */
  function editService($id=''){
  $this->assign('langs',$this->service->model('Lang')->getList(['status'=>1]));
  $data=$this->service->model('Service')->getInfo(['id'=>$id]);
  $this->assign('data',$data);
  if($this->requestType=='post'){
      $temp=input('post.');
      $temp['update_time']=time();
     
           
      $rel=$this->service->model('Service')->edit($temp,['id'=>$id],'editservice');
      if($rel){
          return $this->success('编辑成功');
      }
      return $this->error($this->service->getError());
  }
    return $this->fetch();
  }
  /* ------------------------------------------------------------------ */
  /*
  * 删除
  */
  function deleteService($id=''){
    if($this->service->model('Service')->delete($id)){
      return $this->success('删除成功！');
    }
    return $this->error('删除失败！');
  }
 
 
}
