<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:69:"D:\wamp\www\dmd\public/../application/admin\view\html\searchForm.html";i:1508920174;}*/ ?>
<div class="layui-tab-content" style="padding:20px;">
  <fieldset class="layui-elem-field">
      <legend>查找</legend>
      <div class="layui-field-box">
        <form class="layui-form" action="">
          <div class="layui-form-item">
          <?php if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 3 );++$i;if($vo['type']=='hidden'): ?>
              <input type="hidden" name="<?php echo $vo['name']; ?>" value="<?php echo input($vo['name'])?:''; ?>">
            <?php elseif($vo['type']=='text'): ?>
              <div class="layui-inline">
                  <label class="layui-form-label"><?php echo !empty($vo['title'])?$vo['title']:''; ?></label>
                  <div class="layui-input-inline">
                      <input type="text" name="<?php echo !empty($vo['name'])?$vo['name']:''; ?>" value="<?php echo input($vo['name'])?:''; ?>" autocomplete="off" class="layui-input">
                  </div>
              </div>
            <?php elseif($vo['type']=='select'): ?>
              <div class="layui-inline">
                  <label class="layui-form-label"><?php echo !empty($vo['title'])?$vo['title']:''; ?></label>
                  <div class="layui-input-inline">
                      <select name="<?php echo $vo['name']; ?>" lay-filter="">
                        <?php if(is_array($vo['option']) || $vo['option'] instanceof \think\Collection || $vo['option'] instanceof \think\Paginator): $i = 0; $__LIST__ = $vo['option'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vov): $mod = ($i % 2 );++$i;?>
                        <option value="<?php echo $vov['value']; ?>" <?php echo input($vo['name'])==$vov['value']?'selected':''; ?>><?php echo $vov['key']; ?></option>
                        <?php endforeach; endif; else: echo "" ;endif; ?>
                      </select>
                  </div>
              </div>
            <?php elseif($vo['type']=='switch'): ?>
              <div class="layui-form-item">
                  <label class="layui-form-label"><?php echo !empty($vo['title'])?$vo['title']:''; ?></label>
                  <div class="layui-input-block">
                      <input type="checkbox" <?php echo input($vo['name'])==1? 'checked': ''; ?> value="1" name="<?php echo $vo['name']; ?>" lay-skin="switch" lay-filter="switchTest" lay-text="是|否">
                  </div>
              </div>
            <?php elseif($vo['type']=='s_e_text'): ?>
              <div class="layui-inline">
                  <label class="layui-form-label"><?php echo !empty($vo['title'])?$vo['title']:''; ?></label>
                  <div class="layui-input-inline">
                      <input type="text" name="<?php echo $vo['s_name']; ?>" value="<?php echo input($vo['s_name'])?:''; ?>" autocomplete="off" class="layui-input">
                  </div>
                  <div class="layui-form-mid">-</div>
                  <div class="layui-input-inline">
                      <input type="text" name="<?php echo $vo['e_name']; ?>" value="<?php echo input($vo['e_name'])?:''; ?>" autocomplete="off" class="layui-input">
                  </div>
              </div>
            <?php elseif($vo['type']=='s_e_date'): ?>
              <div class="layui-inline">
                  <label class="layui-form-label"><?php echo !empty($vo['title'])?$vo['title']:''; ?></label>
                  <div class="layui-input-inline">
                      <input type="text" name="<?php echo $vo['s_name']; ?>" value="<?php echo input($vo['s_name'])?:''; ?>" id="<?php echo $vo['s_name']; ?>" autocomplete="off" class="layui-input">
                  </div>
                  <div class="layui-form-mid">-</div>
                  <div class="layui-input-inline">
                      <input type="text" name="<?php echo $vo['e_name']; ?>" value="<?php echo input($vo['e_name'])?:''; ?>" id="<?php echo $vo['e_name']; ?>" autocomplete="off" class="layui-input">
                  </div>
              </div>
              <script>
                  layui.use(['laydate'], function() {
                      var laydate = layui.laydate;
                      laydate.render({
                          elem: '#<?php echo $vo['s_name']; ?>'
                      });
                      laydate.render({
                          elem: '#<?php echo $vo['e_name']; ?>'
                      });
                  });
              </script>
            <?php endif; endforeach; endif; else: echo "" ;endif; ?>
          </div>
          <div class="layui-form-item">
              <label class="layui-form-label"></label>
              <div class="layui-input-inline">
                  <button class="layui-btn layui-btn-primary" lay-submit="" lay-filter="search">查找</button>
              </div>
          </div>
        </form>
      </div>
  </fieldset>
</div>
