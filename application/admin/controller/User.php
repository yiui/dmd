<?php
namespace app\admin\controller;
// +----------------------------------------------------------------------
// | 用户管理
// +----------------------------------------------------------------------
  use core\service\User as UserService;
    use think\Session;
class User extends BaseController
{
  
    protected $user_service;
    /* ------------------------------------------------------------------ */
  /*
  * 初始化
  */
  protected function __init(){
    if(!in_array($this->action,['login','logout'])){
      parent::__init();
    }
    $this->user_service = new UserService();
  }
  protected function __auth(){}
  /* -----------------------------------END------------------------------- */
  /*当前用户管理
  * 
  */
   
  function index(){
      $user=$this->service->model('User')->getInfo(['id'=>$this->uid]);
      if($this->requestType==='post'){
          $data=input('post.');
          if(!isset($data['name'])||empty($data['name'])) return $this->fetch("姓名必填");
          if(!isset($data['mobile'])||empty($data['mobile'])) return $this->fetch("手机号不能为空");
          if(isset($data['passsowrd'])||!empty($data['password'])){
              $data['password']=md5($data['password']);
          }else{
              unset($data['password']);
              
          }
          if(isset($data['username'])){
              unset($data['username']);
          }
          $data['edit_time']=time();
          if($this->service->model('User')->edit($data,['id'=>$this->uid])){
              return $this->success('success');
          }
          return $this->error($this->service->getError());
      }
   $this->assign('user',$user);
    return $this->fetch();
  }
  /* ------------------------------------------------------------------ */
  /*
  * 密码登录
  */
 function login($username='',$password=''){
     if(!empty($this->uid)) return $this->redirect('index/index');
     if($this->requestType==='post'){
         if($this->user_service->login($username,$password)){
             return $this->success('success');
         }
         return $this->error($this->user_service->getError());
     }
     
     return $this->fetch();
 }
  /* ------------------------------------------------------------------ */
  /*
  * 用户退出
  */
 function logout(){
   Session::clear();
    return $this->redirect('user/login');
 }
 
}
