<?php

namespace core\model;

// +----------------------------------------------------------------------
// | 菜单
// +----------------------------------------------------------------------


class Menu extends BaseModel {

    protected $table = 'menu';
    protected $rule = [
        
    ];
    protected $scene_rule = [
        'title'=>'require|max:4',
        'url'=>'require|url|max:50',
        'description'=>'require'
    ];
    protected $msg = [
        'title.require'=>'标题必须',
        'title.max'=>'标题不能超过50个字符',
        'url.require'=>'链接必须',
        'url.url'=>'链接格式不正确',
        'url.max'=>'链接不超过50个字符',
        'description'=>'描述内容必须'
        
        
    ];
    protected $scene = [
        'addmenu'=>['title','url','description'],
        'editmenu'=>['title','url','description'],
        
    ];

    public function getStatusAttr($value) {
        return $value == '1' ? '可用' : '禁用';
    }


}
