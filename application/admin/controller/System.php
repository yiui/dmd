<?php
namespace app\admin\controller;

// +----------------------------------------------------------------------
// | 系统配置
// +----------------------------------------------------------------------
use core\service\Database as DatabaseService;
use think\Cache;
class System extends BaseController {

    protected $system_service;

    /* -----------------------------------START------------------------------- */
    /*
     * 初始化
     */

    function __init() {
        parent::__init();
    }
    /* -----------------------------------END------------------------------- */
    /*
     * 菜单管理
     */

    function menu($page = 1, $limit = 30, $lang = '', $create_time = '', $title = '', $status = '', $min_create_time = '', $max_create_time = '') {
        if ($this->requestType === 'post') {
            $condition = [];
            if ($lang !== '') {
                $condition['lang'] = $lang;
            }
            if ($create_time !== '') {
                $condition['create_time'] = $create_time;
            }
            if ($min_create_time !== '') {
                $condition['create_time'] = ['>=', strtotime($min_create_time)];
            }
            if ($max_create_time !== '') {
                $condition['create_time'] = ['<=', strtotime($max_create_time . ' 23:59:59')];
            }
            if ($title !== '') {
                $condition['title'] = $title;
            }
            if ($status !== '') {
                $condition['status'] = $status;
            }
            $res = $this->service->model('Menu')->getPageList($page, $limit, $condition, 'id desc', '');
            if ($res === false)
                return json(['code' => 1, 'msg' => 'error:' . $this->error('error:' . $this->service->getError())]);
            return json(['code' => 0, 'count' => $res['total_count'], 'data' => $res['data'], 'msg' => '']);
        }



        $list = [
                ['type' => 'select', 'title' => '语言', 'name' => 'lang', 'option' => [['key' => '全部', 'value' => ''], ['key' => '中文', 'value' => '1'], ['key' => '英文', 'value' => '2']]],
                ['type' => 's_e_date', 'title' => '创建时间', 's_name' => 'min_create_time', 'e_name' => 'max_create_time'],
                ['type' => 'select', 'title' => '发布状态', 'name' => 'status', 'option' => [['key' => '全部', 'value' => ''], ['key' => '可用', 'value' => '1'], ['key' => '禁用', 'value' => '0']]],
        ];
        $this->assign('searchForm', $this->html_template->searchForm($list));
        $this->assign('addButton', $this->html_template->addButton('添加菜单', 'admin', 'system', 'addMenu'));
        $this->assign('batheDeleteButton', $this->html_template->batheDeleteButton('table', 'admin', 'system', 'deleteMenu'));
        return $this->fetch();
    }

    /* ------------------------------------------------------------------ */
    /*
     * 添加
     */

    function addMenu() {
        $this->assign('langs', $this->service->model('Lang')->getList(['status' => '1']));
        if ($this->requestType == 'post') {
            $data = input('post.');
            if (!isset($data['title']) || empty($data['title']))
                return $this->error('标题不能为空！');
            $data['create_time'] = time();
//            // 数据验证
//            $result = $this->validate($data, 'addmenu');
//            if (true !== $result) {
//                return $this->error($result);
//            }
            $rel = $this->service->model('Menu')->add($data,'addmenu');
            if ($rel) {
                return $this->success('添加成功');
            }
            return $this->error($this->service->getError());
        }

        return $this->fetch();
    }

    /* ------------------------------------------------------------------ */
    /*
     * 
     * 编辑
     */

    function editMenu($id = '') {
        $this->assign('langs', $this->service->model('Lang')->getList(['status' => 1]));
        $data = $this->service->model('Menu')->getInfo(['id' => $id]);
        $this->assign('data', $data);
        if ($this->requestType == 'post') {
            $temp = input('post.');
            $rel = $this->service->model('menu')->edit($temp, ['id' => $id],'editmenu');
            if ($rel) {
                return $this->success('修改成功');
            }
            return $this->error($this->service->getError());
        }
        return $this->fetch();
    }

    /* ------------------------------------------------------------------ */
    /*
     * 删除
     */

    function deleteMenu($id = '') {
        if ($this->service->model('Menu')->delete($id)) {
            return $this->success('删除成功！');
        }
        return $this->error('删除失败！');
    }

    /* -----------------------------------END------------------------------- */
    /*
     * 公众号管理
     */

    function wechat($page = 1, $limit = 30, $lang = '', $cover = '', $name = '', $id = '', $create_time = '', $title = '', $min_create_time = '', $max_create_time = '', $is_pass = '', $status = '') {
        if ($this->requestType === 'post') {
            $condition = [];
            if ($lang !== '') {
                $condition['lang'] = $lang;
            }
            if ($id !== '') {
                $condition['id'] = $id;
            }
            if ($name !== '') {
                $condition['name'] = $name;
            }
            if ($create_time !== '') {
                $condition['create_time'] = $create_time;
            }
            if ($min_create_time !== '') {
                $condition['create_time'] = ['>=', strtotime($min_create_time)];
            }
            if ($max_create_time !== '') {
                $condition['create_time'] = ['<=', strtotime($max_create_time . ' 23:59:59')];
            }
            if ($status !== '') {
                $condition['status'] = $status;
            }
            $res = $this->service->model('Team')->getPageList($page, $limit, $condition, 'id desc', '');
            if ($res === false)
                return json(['code' => 1, 'msg' => 'error:' . $this->error('error:' . $this->service->getError())]);
            return json(['code' => 0, 'count' => $res['total_count'], 'data' => $res['data'], 'msg' => '']);
        }



        $list = [
                ['type' => 'select', 'title' => '语言', 'name' => 'lang', 'option' => [['key' => '全部', 'value' => ''], ['key' => '中文', 'value' => '1'], ['key' => '英文', 'value' => '2']]],
                ['type' => 's_e_date', 'title' => '创建时间', 's_name' => 'min_create_time', 'e_name' => 'max_create_time'],
                ['type' => 'select', 'title' => '发布状态', 'name' => 'status', 'option' => [['key' => '全部', 'value' => ''], ['key' => '可用', 'value' => '1'], ['key' => '禁用', 'value' => '0']]],
        ];
        $this->assign('searchForm', $this->html_template->searchForm($list));
        $this->assign('addButton', $this->html_template->addButton('添加律师', 'admin', 'team', 'addTeam'));
        $this->assign('batheDeleteButton', $this->html_template->batheDeleteButton('table', 'admin', 'team', 'deleteTeam'));
        return $this->fetch();
    }

    /* ------------------------------------------------------------------ */
    /*
     * 
     * 添加
     */

    function addWechat() {

        return $this->fetch();
    }

    /* ------------------------------------------------------------------ */
    /*
     * 
     * 编辑
     */

    function editWechat($id = '') {

        return $this->fetch();
    }

    /* ------------------------------------------------------------------ */
    /*
     * 删除
     */

    function deleteWechat($id = '') {
        if ($this->service->model('Team')->delete($id)) {
            return $this->success('删除成功！');
        }
        return $this->error('删除失败！');
    }

    /* -----------------------------------END------------------------------- */
    /*
     * 用户管理
     */

    function users($page = 1, $limit = 30, $lang = '', $username = '', $create_time = '', $min_create_time = '', $max_create_time = '', $is_pass = '', $status = '') {
        if ($this->requestType === 'post') {
            $condition = [];
            if ($username !== '') {
                $condition['username'] = $username;
            }
            if ($create_time !== '') {
                $condition['create_time'] = $create_time;
            }
            if ($min_create_time !== '') {
                $condition['create_time'] = ['>=', strtotime($min_create_time)];
            }
            if ($max_create_time !== '') {
                $condition['create_time'] = ['<=', strtotime($max_create_time . ' 23:59:59')];
            }
            if ($status !== '') {
                $condition['status'] = $status;
            }
            $res = $this->service->model('User')->getPageList($page, $limit, $condition, 'id desc', '');
            if ($res === false)
                return json(['code' => 1, 'msg' => 'error:' . $this->error('error:' . $this->service->getError())]);
            return json(['code' => 0, 'count' => $res['total_count'], 'data' => $res['data'], 'msg' => '']);
        }



        $list = [
                ['type' => 's_e_date', 'title' => '创建时间', 's_name' => 'min_create_time', 'e_name' => 'max_create_time'],
                ['type' => 'select', 'title' => '发布状态', 'name' => 'status', 'option' => [['key' => '全部', 'value' => ''], ['key' => '可用', 'value' => '1'], ['key' => '禁用', 'value' => '0']]],
        ];
        $this->assign('searchForm', $this->html_template->searchForm($list));
        $this->assign('addButton', $this->html_template->addButton('添加用户', 'admin', 'system', 'addusers'));
        $this->assign('batheDeleteButton', $this->html_template->batheDeleteButton('table', 'admin', 'system', 'deleteUsers'));
        return $this->fetch();
    }

    /* ------------------------------------------------------------------ */
    /*
     * 添加
     */

    function addUsers() {
        if ($this->requestType == 'post') {
            $data = input('post.');
            $data['password'] = md5($data['password']);
            $data['create_time'] = time();
            $data['birth'] = strtotime($data['birth']);
            $rel = $this->service->model('User')->add($data,'addusers');
            if ($rel) {
                return $this->success('添加成功');
            }
            return $this->error($this->service->getError());
        }

        return $this->fetch();
    }

    /* ------------------------------------------------------------------ */
    /*
     * 
     * 编辑
     */

    function editUsers($id = '') {
        $data = $this->service->model('User')->getInfo(['id' => $id]);
        $this->assign('data', $data);
        if ($this->requestType == 'post') {
            $temp = input('post.');
            if (isset($temp['password']) && !empty($temp['password'])) {
                $temp['password'] = md5($temp['password']);
            } else {
                unset($temp['password']);
            }
            $temp['birth'] = strtotime($temp['birth']);
            $temp['update_time'] = time();
            $rel = $this->service->model('User')->edit($temp, ['id' => $id],'editusers');
            if ($rel) {
                return $this->success('success');
            }
            return $this->error($this->service->getError());
        }

        return $this->fetch();
    }
    /* ------------------------------------------------------------------ */
    /*
     * 删除
     */

    function deleteUsers($id = '') {
        if ($this->service->model('User')->delete($id)) {
            return $this->success('删除成功！');
        }
        return $this->error('删除失败！');
    }
     /* -----------------------------------END------------------------------- */
   
    /*
     * 用户组管理
     * 
     */
    function groups (){
        
        return $this->fetch();
    }
    
    //添加
     function addgroups (){
        
        return $this->fetch();
    }
    
    //修改
     function editgroups (){
        
        return $this->fetch();
    }
    //删除
     function deletegroups (){
        
        return $this->fetch();
    }
      /*
     * 用户规则管理
     * 
     */
  
         function rules($page = 1, $limit = 30) {
        if ($this->requestType === 'post') {
            $condition = [];
            $res = $this->service->model('UserRule')->getPageList($page, $limit, $condition, 'id desc', '');
            if ($res === false)
                return json(['code' => 1, 'msg' => 'error:' . $this->error('error:' . $this->service->getError())]);
            return json(['code' => 0, 'count' => $res['total_count'], 'data' => $res['data'], 'msg' => '']);
        }
        $this->assign('addButton', $this->html_template->addButton('添加规则', 'admin', 'system', 'addrules'));
        $this->assign('batheDeleteButton', $this->html_template->batheDeleteButton('table', 'admin', 'system', 'deleterules'));
        return $this->fetch();
    }
    
    //添加
     function addrules (){
         $data=db('user_rule')->select();
         if($this->requestType=='post'){
             $temp=input('post.');
             $res=$this->service->model('UserRule')->add($temp,'adduserrules');
             if($res){
                 $this->success('添加成功!','rules');
             }else{
                 $this->error($this->service->getError());
             }
           
             return;
         }
         
         $this->assign('data',$data);
  
        
        return $this->fetch();
    }
    
    //修改
     function editrules ($id=""){
         $list=db('user_rule')->select();
           $info= $this->service->model('UserRule')->getInfo(['id' => $id]);
         $this->assign(
                 [
                     'list'=>$list,
                     'info'=>$info,
                 ]);
        
        return $this->fetch();
    }
    //删除
     function deleterules (){
        
        return $this->fetch();
    }

    /* -----------------------------------END------------------------------- */
    /*
     * 广告管理
     */

    function ad($page = 1, $limit = 30, $cid = '', $name = '', $create_time = '', $min_create_time = '', $max_create_time = '', $status = '', $lang = '') {
        if ($this->requestType === 'post') {
            $condition = [];
            if ($lang !== '') {
                $condition['lang'] = $lang;
            }
            if ($cid !== '') {
                $condition['cid'] = $cid;
            }
            if ($name !== '') {
                $condition['name'] = $name;
            }
            if ($create_time !== '') {
                $condition['create_time'] = $create_time;
            }
            if ($min_create_time !== '') {
                $condition['create_time'] = ['>=', strtotime($min_create_time)];
            }
            if ($max_create_time !== '') {
                $condition['create_time'] = ['<=', strtotime($max_create_time . ' 23:59:59')];
            }
            if ($status !== '') {
                $condition['status'] = $status;
            }
            $res = $this->service->model('Ad')->getPageList($page, $limit, $condition, 'id desc', '');
            if ($res === false)
                return json(['code' => 1, 'msg' => 'error:' . $this->error('error:' . $this->service->getError())]);
            return json(['code' => 0, 'count' => $res['total_count'], 'data' => $res['data'], 'msg' => '']);
        }



        $list = [
                ['type' => 'select', 'title' => '语言', 'name' => 'lang', 'option' => [['key' => '全部', 'value' => ''], ['key' => '中文', 'value' => '1'], ['key' => '英文', 'value' => '2']]],
                ['type' => 's_e_date', 'title' => '创建时间', 's_name' => 'min_create_time', 'e_name' => 'max_create_time'],
                ['type' => 'select', 'title' => '发布状态', 'name' => 'status', 'option' => [['key' => '全部', 'value' => ''], ['key' => '可用', 'value' => '1'], ['key' => '禁用', 'value' => '0']]],
        ];
        $this->assign('searchForm', $this->html_template->searchForm($list));
        $this->assign('addButton', $this->html_template->addButton('添加广告', 'admin', 'system', 'addAd'));
        $this->assign('batheDeleteButton', $this->html_template->batheDeleteButton('table', 'admin', 'system', 'deleteAd'));
        return $this->fetch();
    }
    /* ------------------------------------------------------------------ */
    /*
     * 添加
     */

    function addAd() {
        $this->assign('adcategory', $this->service->model('AdCategory')->getList(['status' => 1]));
        if ($this->requestType == 'post') {
            $temp = input('post.');
            $temp['create_time'] = time();
            $rel = $this->service->model('Ad')->add($temp,'addad');
            if ($rel) {
                return $this->success('添加成功');
            }
            return $this->error($this->service->getError());
        }

        return $this->fetch();
    }

    /* ------------------------------------------------------------------ */
    /*
     * 
     * 编辑
     */

    function editAd($id = '') {
        $this->assign('adcategory', $this->service->model('AdCategory')->getList(['status' => 1]));
        $data= $this->service->model('Ad')->getInfo(['id' => $id]);
        $this->assign('data',$data);
        if ($this->requestType == 'post') {
            $temp = input('post.');
             if(empty($temp['name'])){return $this->error('名称不能为空');}
            if($temp['name']==$data['name']){unset($temp['name']);}
           
            $rel = $this->service->model('Ad')->edit($temp, ['id' => $id],'editad');
            if ($rel) {
                return $this->success('success');
            }
            return $this->error($this->service->getError());
        }
        return $this->fetch();
    }

    /* ------------------------------------------------------------------ */
    /*
     * 删除
     */

    function deleteAd($id = '') {
        if ($this->service->model('Ad')->delete($id)) {
            return $this->success('删除成功！');
        }
        return $this->error('删除失败！');
    }

    /* ------------------------------------------------------------------ */
    /*
     * 广告分类管理
     */

    /* ------------------------------------------------------------------ */
    /*
     * 广告列表
     */

    function adCategory($page = 1, $limit = 30, $lang = '', $create_time = '', $title = '', $status = '') {
        if ($this->requestType === 'post') {
            $condition = [];
            $res = $this->service->model('AdCategory')->getPageList($page, $limit, $condition, 'id desc', '');
            if ($res === false)
                return json(['code' => 1, 'msg' => 'error:' . $this->error('error:' . $this->service->getError())]);
            return json(['code' => 0, 'count' => $res['total_count'], 'data' => $res['data'], 'msg' => '']);
        }
        $this->assign('addButton', $this->html_template->addButton('添加分类', 'admin', 'system', 'addadcategory'));
        $this->assign('batheDeleteButton', $this->html_template->batheDeleteButton('table', 'admin', 'system', 'deleteadcategory'));
        return $this->fetch();
    }

    /* -----------------------------------END------------------------------- */
    /*
     * 添加
     */

    function addadcategory() {
        $this->assign('langs', $this->service->model('Lang')->getList(['status' => 1]));
        if ($this->requestType == 'post') {
            $temp = input('post.');
            if ($this->service->model('AdCategory')->add($temp,'addadcategory')) {
                return $this->success('添加成功');
            }
            return $this->error($this->service->getError());
        }
        return $this->fetch();
    }

    function editadCategory($id = '') {
        $this->assign('langs', $this->service->model('Lang')->getList(['status' => 1]));
        $data = $this->service->model('AdCategory')->getInfo(['id' => $id]);
        $this->assign('data', $data);
        if ($this->requestType == 'post') {
            $temp = input('post.');
            if ($this->service->model('AdCategory')->edit($temp, ['id' => $id],'editadcategory')) {
                return $this->success('success');
            }
            return $this->error($this->service->getError());
        }
        return $this->fetch();
    }

    /* -----------------------------------END------------------------------- */
    /*
     * 删除
     */

    function deleteadCategory($id = '') {
        if ($this->service->model('AdCategory')->delete($id)) {
            return $this->success('删除成功');
        }
        return $this->error('删除失败');
    }

    /* -----------------------------------END------------------------------- */
    /*
     * 语言管理
     */

    function lang($page = 1, $limit = 30, $lang = '', $name = '', $id = '', $create_time = '', $title = '', $status = '') {
        if ($this->requestType === 'post') {
            $condition = [];
            $res = $this->service->model('Lang')->getPageList($page, $limit, $condition, 'id desc', '');
            if ($res === false)
                return json(['code' => 1, 'msg' => 'error:' . $this->error('error:' . $this->service->getError())]);
            return json(['code' => 0, 'count' => $res['total_count'], 'data' => $res['data'], 'msg' => '']);
        }
        $this->assign('addButton', $this->html_template->addButton('添加语言', 'admin', 'system', 'addLang'));
        $this->assign('batheDeleteButton', $this->html_template->batheDeleteButton('table', 'admin', 'system', 'deletelang'));
        return $this->fetch();
    }

    /* ------------------------------------------------------------------ */
    /*
     * 添加
     */

    function addLang() {
        if ($this->requestType == 'post') {
            $data = input('post.');
            $data['create_time'] = time();
            if ($this->service->model('Lang')->add($data,'addlang')) {
                return $this->success('success');
            }
            return $this->error($this->service->getError());
        }


        return $this->fetch();
    }

    /* ------------------------------------------------------------------ */
    /*
     * 
     * 编辑
     */

    function editlang($id = '') {
        $data = $this->service->model('Lang')->getInfo(['id' => $id]);
        $this->assign('data', $data);
        if ($this->requestType == 'post') {
            $temp = input('post.');
            if ($this->service->model('Lang')->edit($temp, ['id' => $id],'editlang')) {
                return $this->success('success');
            }
            return $this->error($this->service->getError());
        }
        return $this->fetch();
    }

    /* ------------------------------------------------------------------ */
    /*
     * 删除
     */

    function deleteLang($id = '') {
        if ($this->service->model('Lang')->delete($id)) {
            return $this->success('删除成功！');
        }
        return $this->error('删除失败！');
    }

    /* -----------------------------------END------------------------------- */
    /*
     * 数据库管理
     */
    /* -----------------------------------END------------------------------- */
  /*
  * 管理备份数据
  */
    function databases($page='1',$limit=20){
    if($this->requestType=='post'){
      $database = new DatabaseService();
      $res = $database->pageQuery($page,$limit);
      return json(['code'=>0,'count'=>$res['total_count'],'data'=>$res['data'],'msg'=>'']);
    }
    return $this->fetch();
  }
    /* -----------------------------------END------------------------------- */
  /*
  * 备份数据
  */
    function backupDatabase(){
        $database=new DatabaseService();
        $rel=$database->backup();
        if($res===false) return $this->error($database->getError());
        return $this->success('success');
    }

   /* -----------------------------------END------------------------------- */
  /*
  * 恢复备份数据
  */
  function recoveryDatabase($name=''){
    $database = new DatabaseService();
    $res = $database->recovery($name);
    if($res === false) return $this->error($database->getError());
    return $this->success('success');
  }
    
    /*下载备份数据*/
    function downloadDatabase($name=''){
        $databases=new DatabaseService();
        echo $databases->download($name);
    }
     /*删除备份数据*/
    function deleteDatabase($name=''){
        $database=new DatabaseService();
        $res=$database->delete($name);
        if($res===true) return $this->success('success');
        return $this->error('error');
    }


    /* -----------------------------------END------------------------------- */
    /*
     * 系统配置
     */

    function conf() {
        if ($this->requestType == 'post') {
            $data = input('post.');
            if ($this->service->model('System')->edit($data, ['id' => 1])) {
                $rel = $this->system = $this->service->model('System')->getInfo(['id' => 1]);
                Cache::set('system', $this->system, 3600);
                return $this->success('success');
            }
            return $this->error($this->service->getError());
        }
        return $this->fetch();
    }

    /* -----------------------------end------------------------------------- */
}
