<?php
namespace core\service;
// +----------------------------------------------------------------------
// | 系统配置管理操作
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2017 Even Yin All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Even yin <416467069@qq.com> <13022156261>
// +----------------------------------------------------------------------
use \think\Session as Session;          //用户Session缓存
use \think\Cache   as Cache;            //全局缓存
class User extends BaseService
{
    protected $uid;
    protected $user;
    protected $user_model;
    /* -----------------------------------START------------------------------- */
    /*
    * 构造函数
    */
    public function __construct(){
      parent::__construct();
      $this->user_model = $this->model('User');
    }
    /* -----------------------------------START------------------------------- */
    /*
    * 密码登录
    */
    function login($username='',$password=''){
      $this->user = $this->user_model->getInfo(['username'=>$username]);
      if(empty($this->user)){
        $this->error = '账号错误';
        return false;
      }
      if($this->user['password'] !== md5($password)){
        $this->error = '密码错误';
        return false;
      }
      if($this->user['status']!=1){
        $this->error = '您的账号被禁用了';
        return false;
      }
      $this->uid = $this->user['id'];
      Session::set('uid',$this->uid);
      Session::set('user',$this->user);
      //  $this->setLog($this->uid,1,'账号登录SUCCESS');
      return true;
    }
    /* -----------------------------------END------------------------------- */
    /*
    * 验证码登录
    */
    function mobileLogin($mobile='',$code=''){
      if(!$this->checkMobileCode($mobile,$code)) return false;
      $this->user = $this->user_model->getInfo(['mobile'=>$mobile]);
      if(empty($this->user)){
        $this->error = '手机号不存在！';
        return false;
      }
      $this->uid = $this->user['id'];
      Session::set('uid',$this->uid);
      Session::set('user',$this->user);
      if(!$this->lastLogin($this->uid)){
        $this->setLog($this->uid,0,'更新登录信息ERROR:'.$this->error);
      }
      $this->setLog($this->uid,1,'验证码登录SUCCESS');
      return true;
    }
    /* -----------------------------------END------------------------------- */
    /*
    * 更新登录信息
    */
    function lastLogin($uid){
      $res = $this->user_model->save(['last_login_time'=>strtotime(date('YmdHis')),'last_login_ip'=>$this->ip,'last_login_city'=>$this->city],['id'=>$uid]);
      if($res){
        return true;
      }
      $this->error = $this->user_model->getError();
      return false;
    }
    /* -----------------------------------END------------------------------- */
    /*
    * 退出登录
    */
    function logout(){
      Session::clear();
      return true;
    }
    /* -----------------------------------END------------------------------- */
    /*
    * 修改密码
    */
    function changePwd($password=''){
      if($this->model('UserModel')->validate(['password'=>$password],'changepwd')===false) return false;
      $res  = $this->user_model->save(['password'=>md5($password)],['id'=>$this->getSessionUid()]);
      if($res){
          $this->setLog($this->uid,1,'修改密码SUCCESS');
          return true;
      }
      $this->error = empty($this->user_model->getError())?'未知错误!':$this->user_model->getError();
      $this->setLog($this->uid,0,'修改用户密码ERROR:'.$this->error);
      return false;
    }
    /* -----------------------------------END------------------------------- */
    /*
    * 编辑用户信息
    */
    function editUserInfo($data=[]){
      if($this->user_model->save($data,['id'=>$this->getSessionUid()])){
        $this->user = $this->user_model->getInfo(['id'=>$this->getSessionUid()]);
        Session::set('user',$this->user);
        $this->setLog($this->uid,0,'修改用户资料SUCCESS');
        return true;
      }
      $this->error = $this->user_model->getError();
      $this->setLog($this->uid,0,'修改用户资料ERROR:'.$this->error);
      return false;
    }
    /* -----------------------------------END------------------------------- */
    /*
    * 获取系统微信配置
    */
    function getSessionUid(){
      $this->uid = Session::get('uid');
      return $this->uid;
    }
    function getSessionUserInfo(){
      $this->user = Session::get('user');
      return $this->user;
    }
    /* -----------------------------------END------------------------------- */
    /*
    * 获取用户角色
    */
    function getAuthGroupId(){

    }
    /* -----------------------------------END------------------------------- */
    /*
    * 获取用户角色权限
    */
    function getAuthRuleArray(){

    }
    /* -----------------------------------END------------------------------- */

}
