DROP TABLE IF EXISTS `d_company_news_files`;
CREATE TABLE `d_company_news_files` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `news_id` int(11) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `files` varchar(100) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `create_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='公司信息附件表';
insert into `d_company_news_files` VALUES  ( '5','1','33333333','uploads1201710306a3a9a58080e98cc4935f09d5561bdcc.docx','1','0' ), ( '6','1','77777777777','uploads1201710304597c369880e6cd7a71633c66d1ed030.docx','1','1509347419' ) ;
