<?php
namespace core\model;
// +----------------------------------------------------------------------
// | 新闻分类
// +----------------------------------------------------------------------
class NewsCategory extends BaseModel
{
    protected $table      = 'news_category';
    protected $rule       = [];
    protected $scene_rule = [
        'name'=>'require|unique:NewsCategory|max:20',
    ];
    protected $msg        = [
        'name.require'=>'名称必须',
        'name.unique'=>'该分类已存在',
        'name.max'=>'名称不能超过20个字符',
    ];
    protected $scene      = [
        'addnewscategory'=>['name.require','name.unique','name.max'],
        'editnewscategory'=>['name.unique','name.max'],
        
    ];
}
