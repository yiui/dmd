<?php
namespace core\service;
// +----------------------------------------------------------------------
// | 系统配置管理操作
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2017 Even Yin All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Even yin <416467069@qq.com> <13022156261>
// +----------------------------------------------------------------------
use think\Session as Session;          //用户Session缓存
use think\Cache   as Cache;            //全局缓存
class Upload extends BaseService
{
    protected $src;
    protected $ext;
    /* ------------------------------------------------------------------ */
    /*
    * 上传图片
    */
    function image($src,$path,$file){
      if(!is_dir($path)){
        if(!mkdir( $path , 0755 , true )){
          $this->error = '目录创建失败！';
          return false;
        }
      }
      if(empty($file)){
        $this->error = '上传图片不存在！';
        return false;
      }
      $info = $file->move($path);
	    if($info){
        $this->src = $src.DS.$info->getSaveName();
        return true;
	    }
      $this->error = $file->getError();
      return false;
    }
    /* ------------------------------------------------------------------ */
    /*
    * 上传文件
    */
	  function file($src,$path,$file){
      if(!is_dir($path)){
        if(!mkdir( $path , 0755 , true )){
          $this->error = '目录创建失败！';
          return false;
        }
      }
      if(empty($file)){
        $this->error = '上传文件不存在！';
        return false;
      }
      $info = $file->move($path);
	    if($info){
        $this->ext = $info->getExtension();
        $this->src = $src.DS.$info->getSaveName();
        return true;
	    }
      $this->error = $file->getError();
      return false;
    }
    /* ------------------------------------------------------------------ */
    /*
    * 获取上传成功后的SRC
    */
    function getSrc(){
      return $this->src;
    }
    /* ------------------------------------------------------------------ */
    /*
    * 获取上传成功后的EXT后缀名
    */
    function getExtension(){
      return $this->ext;
    }
    /* ------------------------------------------------------------------ */

}
