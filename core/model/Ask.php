<?php
namespace core\model;
// +----------------------------------------------------------------------
// | 咨询
// +----------------------------------------------------------------------
class Ask extends BaseModel
{
    protected $table      = 'ask';
    protected $rule       = [];
    protected $scene_rule = [
        'name'=>'require|max:20',
        'mobile'=>'require|max:11',
        'content'=>'require'
    ];
    protected $msg        = [
        'name.require'=>'姓名不能为空',
        'name.max'=>'姓名不能超过20个字符',
        'mobile.require'=>'手机号必须',
        'mobile.max'=>'手机格式不正确',
        'content.require'=>'内容必须',
    ];
    protected $scene      = [
        'addask'=>['name','mobile','content'],
        'editask'=>['name','mobile','content'],
    ];
}
