<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:76:"D:\wamp\www\dmd\public/../application/admin\view\html\batheDeleteButton.html";i:1508919811;}*/ ?>
<button class="layui-btn layui-btn-primary" id="batchDelete">批量删除</button>
<script>
  layui.use(['table','jquery'], function(){
    var table = layui.table;
    var $ = layui.jquery;
    $('#batchDelete').on('click',function(){
      var checkStatus = table.checkStatus('<?php echo $tablename; ?>'),data = checkStatus.data;
      if(data.length<1){
          layer.msg('未选中要删除的数据！',{icon:5,anim:6});
          return false;
      }
      var id = new Array();
      for(var i=0;i<data.length;i++){
        id.push(data[i].id);
      }
      id = id.toString();
      layer.confirm('确定要删除ID：'+id, function(index){
        $.post('<?php echo $url; ?>',{id:id},function(res){
          layer.close(index);
          if(res.code==1){
            location.href = location;
          }else{
            layer.msg('失败:'+res.msg,{icon:5,anim:6});
          }
        })
      });
    })
  })
</script>
