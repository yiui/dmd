DROP TABLE IF EXISTS `d_lang`;
CREATE TABLE `d_lang` (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `name` varchar(10) NOT NULL,
  `title` varchar(20) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='语言表';
insert into `d_lang` VALUES  ( '1','zh','中文','0','1' ), ( '2','en','英文','0','1' ), ( '3','france','法语','1509355555','1' ) ;
