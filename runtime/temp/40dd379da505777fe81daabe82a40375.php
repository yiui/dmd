<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:65:"D:\wamp\www\dmd\public/../application/admin\view\system\conf.html";i:1509358886;s:60:"D:\wamp\www\dmd\public/../application/admin\view\layout.html";i:1509011174;}*/ ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width,initial-scale=1, maximum-scale=1, user-scalable=no">
  <title>管理后台|江苏大名大律师事务所</title>
  <link rel="stylesheet" href="__PUBLIC__/layui/css/layui.css">
  <script src="__PUBLIC__/layui/layui.js"></script>
  <style>
    body{background-color: #eee;min-width: 1200px;}
    .container{margin:0 auto;width: 1200px;}
    .head{height: 80px;background-color: #fff;border-bottom: 1px solid #ddd;border-top:5px solid #444;}
    .head .logo{#float:left;background-image: url(http://dmd.qianbeinet.com/dmd_logo.png);background-size: cover;width: 200px;height:40px;margin-top: 20px;float: left;}
    .head .layui-nav{background-color:#fff;border-radius:0;float: right;padding:18px 0;}
    .head .layui-nav .layui-nav-more{display: none}
    .head .layui-nav-child{top:62px;box-shadow:none;}
    .head .layui-nav .layui-nav-item{line-height: 24px;text-align: center;}
    .head .layui-nav .layui-nav-item p{font-size: 12px;}
    .head .layui-nav .layui-nav-item a{color: #7A7A7A;font-size: 13px;padding:10px 36px;}
    .head .layui-nav .layui-nav-item a:hover,.head .layui-nav .layui-this a{color: #fff;background-color: #444;font-size: 13px;}
    .head .layui-nav .layui-this:after, .layui-nav-bar, .layui-nav-tree .layui-nav-itemed:after{height:0;}

    .footer{background-color: #eee;color:rgba(0, 0, 0, 0.5);font:14px Helvetica Neue,Helvetica,PingFang SC,\5FAE\8F6F\96C5\9ED1,Tahoma,Arial,sans-serif;}
    .footer a{color:rgba(255,255,255,.6);line-height: 24px;font-size: 13px;color:rgba(255,255,255,.3);}
    .footer .footer-link-block{min-height: 200px;}
    .footer .footer-link-title{font-size: 16px;color: rgba(255,255,255,.8);margin-bottom: 10px;}

    .layui-btn{background-color: #444;}
    .layui-btn-normal{background-color: #1E9FFF;}
    .layui-btn-danger{background-color: #FF5722;}
    .layui-btn-primary{background-color: #fff;}
    .layui-btn-primary:hover{border-color:#444;}
  </style>
  <style media="screen">
    .teacher{min-height:600px;background-color:#fff;border:1px solid #ddd;}
    .layui-nav-tree .layui-nav-item a:hover{}
      .layui-nav-tree .layui-nav-bar{width: 0}
    .layui-nav-tree .layui-nav-item{line-height: 48px}
    .layui-nav-tree .layui-nav-item a{height: 48px;color: #000;text-align: center;}
    .layui-nav-tree .layui-nav-item a:hover{background-color:#444;}
    .layui-nav-tree.layui-nav{background-color: #fff}
    .layui-nav-tree .layui-nav-child dd.layui-this,
     .layui-nav-tree .layui-nav-child dd.layui-this a,
      .layui-nav-tree .layui-this,
       .layui-nav-tree .layui-this>a,
       .layui-nav-tree .layui-this>a:hover{background-color: #444;}
    .layui-nav-tree .layui-this>a{color: #fff;}

    .teacher .layui-tab-card{border: 0;box-shadow: none;border-radius: 0}
    .teacher .layui-tab{margin: 0}
    .teacher .layui-tab .layui-tab-title{height: 48px;background-color: #fff;}
    .teacher .layui-tab .layui-tab-title li{line-height: 48px;padding:0 65px;}
    .teacher .layui-tab .layui-tab-title li.layui-this a{color: #fff;}
    .teacher .layui-tab .layui-tab-title .layui-this{background-color: #444;color: #fff}
    .teacher .layui-tab .layui-tab-title .layui-this:after{height: 48px;border: 0;}
    .teacher .layui-tab .layui-tab-content{padding: 20px}
    .teacher .layui-tab .layui-tab-title .layui-tab-bar{height: 48px;line-height: 48px;}

    .layui-form-item{margin-bottom: 30px;}
    input:disabled{background-color: #eee}
    .layui-form-select dl dd.layui-this{background-color: #444;}
    .layui-laydate .layui-this{background-color: #444!important;}
    .layui-laypage .layui-laypage-curr .layui-laypage-em{background-color: #444;}
  </style>
  
</head>

<body>
  
    <header class="">
      <div class="head">
        <div class="container">
          <div class="layui-row">
            <div class="layui-col-xs2">
              <a href="<?php echo url('index/index'); ?>" class="logo"></a>
            </div>
            <div class="layui-col-xs10">
              <?php if(!empty($user)): ?>
              <ul class="layui-nav">
                <li class="layui-nav-item"><a href="/" target="_blank">官网首页</a></li>
                <li class="layui-nav-item <?php if(in_array($controller,['index','system'])): ?>layui-this<?php endif; ?>"><a href="<?php echo url('index/index'); ?>">系统管理</a></li>
                <li class="layui-nav-item <?php if(in_array($controller,['news'])): ?>layui-this<?php endif; ?>"><a href="<?php echo url('news/index'); ?>">内容管理</a></li>
                <li class="layui-nav-item <?php if(in_array($controller,['spec'])): ?>layui-this<?php endif; ?>"><a href="<?php echo url('spec/index'); ?>">服务公司管理</a></li>
                <li class="layui-nav-item <?php if(in_array($controller,['user'])): ?>layui-this<?php endif; ?>">
                  <a href="javascript:;">用户中心</a>
                  <dl class="layui-nav-child">
                    <dd><a href="<?php echo url('user/index'); ?>" style="background-color:#fff;color:#7A7A7A">个人信息</a></dd>
                    <dd><a href="<?php echo url('index/clearCache'); ?>" style="background-color:#fff;color:#7A7A7A">更新缓存</a></dd>
                    <dd><a href="<?php echo url('user/logout'); ?>" style="background-color:#fff;color:#7A7A7A">安全退出</a></dd>
                  </dl>
                </li>
                </ul>
              <?php endif; ?>
            </div>
          </div>
        </div>
      </div>
    </header>
  
  
  <div class="container">
    <div class="layui-row layui-col-space10" style="margin-top:5px;">
      <div class="layui-col-xs2">
        <div class="teacher">
          
<style>
    .teacher{
        height:1442px;
    }
   
</style>
<ul class="layui-nav layui-nav-tree" style="width:100%;">
    <li class="layui-nav-item <?php if(in_array(request()->action(),['index'])): ?>layui-this<?php endif; ?>"><a href="<?php echo url('index/index'); ?>">主页</a></li>
    <li class="layui-nav-item <?php if(in_array(request()->action(),['menu'])): ?>layui-this<?php endif; ?>"><a href="<?php echo url('system/menu'); ?>">菜单管理</a></li>
    <li class="layui-nav-item <?php if(in_array(request()->action(),['wechat'])): ?>layui-this<?php endif; ?>"><a href="<?php echo url('system/wechat'); ?>">公众号管理</a></li>
    <li class="layui-nav-item <?php if(in_array(request()->action(),['users'])): ?>layui-this<?php endif; ?>"><a href="<?php echo url('system/users'); ?>">用户管理</a></li>
    <li class="layui-nav-item <?php if(in_array(request()->action(),['ad'])): ?>layui-this<?php endif; ?>"><a href="<?php echo url('system/ad'); ?>">广告图管理</a></li>
    <li class="layui-nav-item <?php if(in_array(request()->action(),['lang'])): ?>layui-this<?php endif; ?>"><a href="<?php echo url('system/lang'); ?>">多语言管理</a></li>
    <li class="layui-nav-item <?php if(in_array(request()->action(),['databases'])): ?>layui-this<?php endif; ?>"><a href="<?php echo url('system/databases'); ?>">数据库管理</a></li>
    <li class="layui-nav-item <?php if(in_array(request()->action(),['conf'])): ?>layui-this<?php endif; ?>"><a href="<?php echo url('system/conf'); ?>">系统配置</a></li>
</ul>

        </div>
      </div>
      <div class="layui-col-xs10">
        <div class="teacher">
          
<div class="layui-tab layui-tab-brief" style="margin:0;box-shadow:none;border:0;">
    <ul class="layui-tab-title">
        <li <?php echo request()->action()=='conf'?'class="layui-this"':''; ?>  onclick="location.href='<?php echo url('system/conf'); ?>'">网站信息</li>
    </ul>
    <div class="layui-tab-content" >
        <div class="lyui-tab-item layui-show">
            <form class="layui-form" action="" method="post" style="padding:30px 140px 30px 30px;" >

                <div class="layui-form-item">
                    <div class="layui-block">
                        <label class="layui-form-label">网站名称</label>
                        <div class="layui-input-block">
                            <input type="text" name="name" value="<?php echo !empty($system['web_name'])?$system['web_name']:''; ?>"  placeholder="" required lay-verify="required" autocomplete="off" class="layui-input">
                        </div>
                    </div>
                </div>

                <div class="layui-form-item">
                    <div class="layui-inline">
                        <label class="layui-form-label">主Logo</label>
                        <div class="layui-upload layui-input-inline">
                            <button type="button" class="layui-btn layui-btn-primary" id="logo">上传图片</button><input class="layui-upload-file" type="file" name="file">
                            <div class="layui-upload-list">
                                <img class="layui-upload-img" src="__PUBLIC__/<?php echo !empty($system['logo'])?$system['logo']:'/static/image/upload.png'; ?>" id="logo-img" width="340" height="68">
                                <input type="hidden" name="logo" id="logo-val" value="<?php echo !empty($system['logo'])?$system['logo']:''; ?>" lay-verify="required" autocomplete="off" class="layui-input">
                            </div>
                            <div id="logo-err"></div>
                        </div>
                        <script>
                            layui.use(['jquery', 'upload'], function(){
                            var $ = layui.jquery;
                            var upload = layui.upload;
                            var uploadInst = upload.render({
                            elem: '#logo',
                                    url: '<?php echo url('upload / image'); ?>',
                                    size:2 * 1024,
                                    before: function(obj){
                                    },
                                    done: function(res){
                                    if (res.code == 1){
                                    $('#logo-img').attr('src', res.src);
                                    $('#logo-img').css('display', 'block');
                                    $('#logo-val').val(res.src);
                                    return true;
                                    }
                                    },
                                    error: function(){
                                    var errText = $('#logo-err');
                                    errText.html('<span style="color: #FF5722;">上传失败</span> <a class="layui-btn layui-btn-mini logo-reload">重试</a>');
                                    errText.find('.logo-reload').on('click', function(){
                                    uploadInst.upload();
                                    });
                                    }
                            });
                            });
                        </script>
                    </div>
                </div>

                <div class="layui-form-item">
                    <div class="layui-inline">
                        <label class="layui-form-label">副Logo</label>
                        <div class="layui-upload layui-input-inline">
                            <button type="button" class="layui-btn layui-btn-primary" id="flogo">上传图片</button><input class="layui-upload-file" type="file" name="file">
                            <div class="layui-upload-list">
                                <img class="layui-upload-img" src="__ROOT__/<?php echo !empty($system['flogo'])?$system['flogo']:'/static/image/upload.png'; ?>" id="flogo-img" width="64" height="64">
                                <input type="hidden" name="flogo" id="flogo-val" value="<?php echo !empty($system['flogo'])?$system['flogo']:''; ?>" lay-verify="required" autocomplete="off" class="layui-input">
                            </div>
                            <div id="flogo-err"></div>
                        </div>
                        <script>
                            layui.use(['jquery', 'upload'], function(){
                            var $ = layui.jquery;
                            var upload = layui.upload;
                            var uploadInst = upload.render({
                            elem: '#flogo',
                                    url: '<?php echo url('upload / image'); ?>',
                                    size:2 * 1024,
                                    before: function(obj){
                                    },
                                    done: function(res){
                                    if (res.code == 1){
                                    $('#flogo-img').attr('src', res.src);
                                    $('#flogo-img').css('display', 'block');
                                    $('#flogo-val').val(res.src);
                                    return true;
                                    }
                                    },
                                    error: function(){
                                    var errText = $('#flogo-err');
                                    errText.html('<span style="color: #FF5722;">上传失败</span> <a class="layui-btn layui-btn-mini flogo-reload">重试</a>');
                                    errText.find('.flogo-reload').on('click', function(){
                                    uploadInst.upload();
                                    });
                                    }
                            });
                            });
                        </script>
                    </div>
                </div>

                <div class="layui-form-item">
                    <div class="layui-block">
                        <label class="layui-form-label">网站域名</label>
                        <div class="layui-input-block">
                            <input type="text" name="domain" value="<?php echo !empty($system['domain'])?$system['domain']:''; ?>" placeholder="" required lay-verify="required" autocomplete="off" class="layui-input">
                        </div>
                    </div>
                </div>
                <div class="layui-form-item">
                    <div class="layui-block">
                        <label class="layui-form-label">icp备案号</label>
                        <div class="layui-input-block">
                            <input type="text" name="icp" value="<?php echo !empty($system['icp'])?$system['icp']:''; ?>" placeholder="" required lay-verify="required" autocomplete="off" class="layui-input">
                        </div>
                    </div>
                </div>
                <div class="layui-form-item">
                    <div class="layui-block">
                        <label class="layui-form-label">版权信息</label>
                        <div class="layui-input-block">
                            <input type="text" name="copyright" value="<?php echo !empty($system['copyright'])?$system['copyright']:''; ?>" placeholder="" required lay-verify="required" autocomplete="off" class="layui-input">
                        </div>
                    </div>
                </div>
                <div class="layui-form-item">
                    <div class="layui-block">
                        <label class="layui-form-label">公司名称</label>
                        <div class="layui-input-block">
                            <input type="text" name="company" value="<?php echo !empty($system['company_name'])?$system['company_name']:''; ?>" placeholder="" required lay-verify="required" autocomplete="off" class="layui-input">
                        </div>
                    </div>
                </div>
                <div class="layui-form-item">
                    <div class="layui-block">
                        <label class="layui-form-label">公司介绍</label>
                        <div class="layui-input-block">
                            <textarea type="text" name="introduce" placeholder="" required lay-verify="required" class="layui-textarea"><?php echo !empty($system['introduce'])?$system['introduce']:''; ?></textarea>
                        </div>
                    </div>
                </div>

                
                   <div class="layui-form-item">
                    <div class="layui-block">
                        <label class="layui-form-label">关于我们</label>
                        <div class="layui-input-block">
                            <textarea type="text" name="about" placeholder=""  required lay-verify="required" class="layui-textarea"><?php echo !empty($system['about'])?$system['about']:''; ?></textarea>
                        </div>
                    </div>
                </div>
               

                <div class="layui-form-item">
                    <div class="layui-block">
                        <label class="layui-form-label">公司地址</label>
                        <div class="layui-input-block">
                            <input type="text" name="addr" value="<?php echo !empty($system['addr'])?$system['addr']:''; ?>" placeholder="" required lay-verify="required" autocomplete="off" class="layui-input">
                        </div>
                    </div>
                </div>
                <div class="layui-form-item">
                    <div class="layui-block">
                        <label class="layui-form-label">联系人</label>
                        <div class="layui-input-block">
                            <input type="text" name="uname" value="<?php echo !empty($system['contacts'])?$system['contacts']:''; ?>" placeholder="" required lay-verify="required" autocomplete="off" class="layui-input">
                        </div>
                    </div>
                </div>
                <div class="layui-form-item">
                    <div class="layui-block">
                        <label class="layui-form-label">电话</label>
                        <div class="layui-input-block">
                            <input type="text" name="tel" value="<?php echo !empty($system['mobile'])?$system['mobile']:''; ?>" placeholder="" required lay-verify="required" autocomplete="off" class="layui-input">
                        </div>
                    </div>
                </div>
                <div class="layui-form-item">
                    <div class="layui-block">
                        <label class="layui-form-label">Email</label>
                        <div class="layui-input-block">
                            <input type="text" name="email" value="<?php echo !empty($system['email'])?$system['email']:''; ?>" placeholder="" required lay-verify="required" autocomplete="off" class="layui-input">
                        </div>
                    </div>
                </div>

                <div class="layui-form-item">
                    <div class="layui-inline">
                        <label class="layui-form-label">状态</label>
                        <div class="layui-input-inline">
                            <select name="status">
                                <option value="1">开启访问</option>
                                <option value="0">关闭访问</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="layui-form-item">
                    <div class="layui-block">
                        <div class="layui-input-block">
                            <button class="layui-btn" lay-submit lay-filter="*" type="button">保存修改</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

    </div>

</div>

<script>
    layui.use(['layer', 'jquery', 'form','layedit'], function(){
    var $ = layui.jquery;
    var form = layui.form;
    var layedit=layui.layedit;
    layedit.set({
    	uploadImage:{
    		url: '<?php echo url('upload/articleImage'); ?>',type:'post'
    	}
    })
    var editIndex = layedit.build('content');
    //监听提交
    form.on('submit(*)', function(data){
    data.form.submit();
    return true;
    });
    });
</script>

        </div>
      </div>
    </div>
  </div>
  
    <footer class="footer">
      <div class="container">
        <div style="padding:30px 0;">
          <div style="clear:both;text-align:center;">Copyright © 2017 <?php echo request()->domain(); ?> 版权所有</div>
        </div>
      </div>
    </footer>
  
  <script>
      layui.use(['element'],function(){
        var element = layui.element;
      });
  </script>
  
</body>
</html>
