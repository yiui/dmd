DROP TABLE IF EXISTS `d_ad`;
CREATE TABLE `d_ad` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cid` int(2) NOT NULL,
  `name` varchar(20) NOT NULL,
  `cover` varchar(100) NOT NULL,
  `description` text,
  `status` int(1) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  `lang` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COMMENT='广告表';
insert into `d_ad` VALUES  ( '2','2','广告1','uploads12017103171edd6d8e8c5852c660e9cea81aa800d.jpg','描述11','1','1509438142','1' ), ( '3','1','风景','uploads1201710315a5139151ce85ff6843d7ccbbaae08fc.jpg','描述','1','1509440082','1' ), ( '4','2','广告3','uploads120171101976e7549eae897c495efe32bbe99ecf4.jpg','描述333','0','1509501643','1' ), ( '6','2','33333','uploads12017110114af9f2340e361c3320b38ed702d1c5b.jpg','33','1','1509506925','1' ), ( '7','1','名可名','\uploads\1\20171102\44fba029582c1e46b6d5c623fcbe030a.jpg','描述','1','1509603725','1' ), ( '8','1','道可道','\uploads\1\20171102\693746e3fcdbff05f7725ff633dd7948.jpg','描述','1','1509603761','1' ), ( '9','2','铭刻','\uploads\1\20171102\fc05a772d8531f4397f0e5472e0666e5.jpg','描述','1','1509603802','1' ), ( '10','1','轮播图','\uploads\1\20171102\23421c2dde9e13322b1a07fd348e2731.jpg','描述','1','1509604069','1' ), ( '11','1','民民','\uploads\1\20171102\7b5586b0c3fef8baefcb431e6b35767e.jpg','描述','1','1509604153','1' ), ( '12','1','慢慢','\uploads\1\20171102\d20e2749c90201a8d0153927c63a0a30.jpg','描述','1','1509604251','1' ) ;
