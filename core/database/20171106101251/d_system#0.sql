DROP TABLE IF EXISTS `d_system`;
CREATE TABLE `d_system` (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `web_name` varchar(50) DEFAULT NULL,
  `flogo` varchar(200) DEFAULT NULL,
  `logo` varchar(200) DEFAULT NULL,
  `domain` varchar(100) DEFAULT NULL,
  `contacts` varchar(100) DEFAULT NULL,
  `icp` varchar(100) DEFAULT NULL,
  `copyright` varchar(100) DEFAULT NULL,
  `company_name` varchar(100) DEFAULT NULL,
  `introduce` text,
  `about` text,
  `addr` varchar(100) DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `lang` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='系统配置表';
insert into `d_system` VALUES  ( '1','大名律师','uploads120171030�f502dc82796cc93bfa1db04a82068bb.jpg','uploads120171030472b3912c3bfd868da45c3b549cc89ee.jpg','www.dmd.ccom','联系人','icp11111111111111','版权归作者所有。','大明律师','这是一个高大上的网站222','关于我们。。。。','地址','19822222222','123456@126.com','1','1','1','1' ) ;
