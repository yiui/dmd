<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:69:"D:\wamp\www\dmd\public/../application/admin\view\system\addrules.html";i:1511754180;s:60:"D:\wamp\www\dmd\public/../application/admin\view\layout.html";i:1511749434;}*/ ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width,initial-scale=1, maximum-scale=1, user-scalable=no">
  <title>管理后台</title>
  <link rel="stylesheet" href="__PUBLIC__/layui/css/layui.css">
  <script src="__PUBLIC__/layui/layui.js"></script>
  <style>
    body{background-color: #eee;min-width: 1200px;}
    .container{margin:0 auto;width: 1200px;}
    .head{height: 80px;background-color: #fff;border-bottom: 1px solid #ddd;border-top:5px solid #444;}
    .head .logo{#float:left;background-image: url(#http://dmd.qianbeinet.com/dmd_logo.png);background-size: cover;width: 200px;height:40px;margin-top: 20px;float: left;}
    .head .layui-nav{background-color:#fff;border-radius:0;float: right;padding:18px 0;}
    .head .layui-nav .layui-nav-more{display: none}
    .head .layui-nav-child{top:62px;box-shadow:none;}
    .head .layui-nav .layui-nav-item{line-height: 24px;text-align: center;}
    .head .layui-nav .layui-nav-item p{font-size: 12px;}
    .head .layui-nav .layui-nav-item a{color: #7A7A7A;font-size: 13px;padding:10px 36px;}
    .head .layui-nav .layui-nav-item a:hover,.head .layui-nav .layui-this a{color: #fff;background-color: #444;font-size: 13px;}
    .head .layui-nav .layui-this:after, .layui-nav-bar, .layui-nav-tree .layui-nav-itemed:after{height:0;}

    .footer{background-color: #eee;color:rgba(0, 0, 0, 0.5);font:14px Helvetica Neue,Helvetica,PingFang SC,\5FAE\8F6F\96C5\9ED1,Tahoma,Arial,sans-serif;}
    .footer a{color:rgba(255,255,255,.6);line-height: 24px;font-size: 13px;color:rgba(255,255,255,.3);}
    .footer .footer-link-block{min-height: 200px;}
    .footer .footer-link-title{font-size: 16px;color: rgba(255,255,255,.8);margin-bottom: 10px;}

    .layui-btn{background-color: #444;}
    .layui-btn-normal{background-color: #1E9FFF;}
    .layui-btn-danger{background-color: #FF5722;}
    .layui-btn-primary{background-color: #fff;}
    .layui-btn-primary:hover{border-color:#444;}
  </style>
  <style media="screen">
    .teacher{min-height:600px;background-color:#fff;border:1px solid #ddd;}
    .layui-nav-tree .layui-nav-item a:hover{}
      .layui-nav-tree .layui-nav-bar{width: 0}
    .layui-nav-tree .layui-nav-item{line-height: 48px}
    .layui-nav-tree .layui-nav-item a{height: 48px;color: #000;text-align: center;}
    .layui-nav-tree .layui-nav-item a:hover{background-color:#444;}
    .layui-nav-tree.layui-nav{background-color: #fff}
    .layui-nav-tree .layui-nav-child dd.layui-this,
     .layui-nav-tree .layui-nav-child dd.layui-this a,
      .layui-nav-tree .layui-this,
       .layui-nav-tree .layui-this>a,
       .layui-nav-tree .layui-this>a:hover{background-color: #444;}
    .layui-nav-tree .layui-this>a{color: #fff;}

    .teacher .layui-tab-card{border: 0;box-shadow: none;border-radius: 0}
    .teacher .layui-tab{margin: 0}
    .teacher .layui-tab .layui-tab-title{height: 48px;background-color: #fff;}
    .teacher .layui-tab .layui-tab-title li{line-height: 48px;padding:0 65px;}
    .teacher .layui-tab .layui-tab-title li.layui-this a{color: #fff;}
    .teacher .layui-tab .layui-tab-title .layui-this{background-color: #444;color: #fff}
    .teacher .layui-tab .layui-tab-title .layui-this:after{height: 48px;border: 0;}
    .teacher .layui-tab .layui-tab-content{padding: 20px}
    .teacher .layui-tab .layui-tab-title .layui-tab-bar{height: 48px;line-height: 48px;}

    .layui-form-item{margin-bottom: 30px;}
    input:disabled{background-color: #eee}
    .layui-form-select dl dd.layui-this{background-color: #444;}
    .layui-laydate .layui-this{background-color: #444!important;}
    .layui-laypage .layui-laypage-curr .layui-laypage-em{background-color: #444;}
  </style>
  
<style>
body{min-width: inherit;background-color: #fff;overflow-x: hidden;}
.teacher{display: none;}
.layui-layout-admin .layui-body{top:10px;}
form{padding:50px 0;}
.layui-footer{width: 100%;position: fixed;bottom: 0;padding:10px 0px;background-color: #eee;text-align: center;}
</style>

</head>

<body>
  
  
<form class="layui-form" action="" method="post">
    <div class="layui-form-item">
    <div class="layui-inline">
      <label class="layui-form-label">上级名称</label>
      <div class="layui-input-inline">
        <select name="pid" required lay-verify="required" lay-filter="">
            <option value="0"> 根目录</option>
            <?php if(is_array($data) || $data instanceof \think\Collection || $data instanceof \think\Paginator): $i = 0; $__LIST__ = $data;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
          <option value="<?php echo $vo['id']; ?>" ><?php echo $vo['title']; ?></option>
          <?php endforeach; endif; else: echo "" ;endif; ?>
        </select>
      </div>
    </div>
  </div>

   
  
  
  <div class="layui-form-item">
    <div class="layui-inline">
      <label class="layui-form-label">权限名称</label>
      <div class="layui-input-inline">
        <input type="text" name="title" placeholder="" required lay-verify="title" autocomplete="off" class="layui-input">
      </div>
    </div>
  </div>
    
    <div class="layui-form-item">
    <div class="layui-inline">
      <label class="layui-form-label">控/方</label>
      <div class="layui-input-inline">
        <input type="text" name="name" placeholder="" required lay-verify="name" autocomplete="off" class="layui-input">
      </div>
    </div>
  </div>
  
  

  
  

  <div class="layui-footer">
    <button class="layui-btn layui-btn-normal layui-btn-small" lay-submit lay-filter="*" type="button">立即提交</button>
    <button class="layui-btn layui-btn-small layui-btn-primary" id="closeFrame" type="button">完成/取消</button>
  </div>

</form>
<script>
  layui.use(['layer','jquery','form','layedit','laydate'],function(){
    var $ = layui.jquery;
    var form = layui.form;
    var layedit = layui.layedit;
    var laydate=layui.laydate;
    

    layedit.set({
    	uploadImage:{
    		url: '<?php echo url('upload/articleImage'); ?>',type:'post'
    	}
    })

    var editIndex = layedit.build('content');

    form.verify({
//      username:function(value){if(value.length < 3){return '账号至少得3个字符啊';}}
//       ,pass: [/(.+){6,12}$/, '密码必须6到12位']
//        ,name: [/^[\u4e00-\u9fa5]{2,5}$/, '姓名必须为2到5个汉字']
        
      
    });
    
    //日期
  laydate.render({
    elem: '#date'
  });
 

    //监听提交
    form.on('submit(*)', function(data){
      data.form.submit();
      return true;
    });
    $('#closeFrame').on('click',function(){
      parent.layer.closeAll();
    })
    $('#addData').on('click',function(){
      $('form').submit();
    })
  });
</script>

  <div class="container">
    <div class="layui-row layui-col-space10" style="margin-top:5px;">
      <div class="layui-col-xs2">
        <div class="teacher">
          
        </div>
      </div>
      <div class="layui-col-xs10">
        <div class="teacher">
          
        </div>
      </div>
    </div>
  </div>
  


  <script>
      layui.use(['element'],function(){
        var element = layui.element;
      });
  </script>
  

</body>
</html>
