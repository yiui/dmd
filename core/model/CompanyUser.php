<?php
namespace core\model;
// +----------------------------------------------------------------------
// | 公司用户表
// +----------------------------------------------------------------------
class CompanyUser extends BaseModel
{
    protected $table      = 'company_user';
    protected $rule       = [];
    protected $scene_rule = [
        'company_id'=>'require|unique:CompanyUser',
        'username'=>'require|unique:CompanyUser|max:20',
        
    ];
    protected $msg        = [
        'company_id.require'=>'公司名称已存在！',
        'company_id.unique'=>'该公司已有账户',
        'username.require'=>'用户名必须',
        'username.unique'=>'用户名必须唯一',
        'username.max'=>'用户名不能超过20个字符',
        
    ];
    protected $scene      = [
        'addcompanyuser'=>['company_id','username'],
        'editcompanyuser'=>['username.unique','username.max'],
    ];
    
       protected function getCompanyIdAttr($value){
        $rel=db('company')->where('id',$value)->find();
        if($rel){
            return $rel['name'];
        }
        return null;
    }
    
    
}
