DROP TABLE IF EXISTS `d_cases`;
CREATE TABLE `d_cases` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `cover` varchar(100) DEFAULT NULL,
  `description` text,
  `content` text,
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `is_public` int(1) DEFAULT NULL,
  `lang` int(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='案例表';
insert into `d_cases` VALUES  ( '1','1111','\uploads\1\20171103\ece05ae29c3bee4edd09bc44135d40a5.jpg','11','','1509677448','1509761856','1','','1' ), ( '2','6666666666','\uploads\1\20171106\e8a7ea21cf40cc34e6a1e465cc4b7160.jpg','财经一习谈| 这四年，精准扶贫实现历史跨越','<h1>财经一习谈| 这四年，精准扶贫实现历史跨越</h1>','1509931935','1509931952','1','','1' ) ;
