<?php
namespace app\admin\controller;
// +----------------------------------------------------------------------
// | 团队管理
// +----------------------------------------------------------------------

class Team extends BaseController
{
  /* -----------------------------------END------------------------------- */
  /*
  * 团队管理
  */
  function index($page=1,$limit=30,$lang='',$name='',$create_time='',$min_create_time='',$max_create_time='',$status=''){
    if($this->requestType === 'post'){
      $condition = [];
      if($lang!==''){$condition['lang']=$lang;}
      if($name!==''){$condition['name']=$name;}
      if($create_time!==''){$condition['create_time']=$create_time;}
      if($min_create_time!==''){$condition['create_time']=['>=',strtotime($min_create_time)];}
      if($max_create_time!==''){$condition['create_time']=['<=',strtotime($max_create_time.' 23:59:59')];}
      if($status!==''){$condition['status']=$status;}
      $res = $this->service->model('Team')->getPageList($page,$limit,$condition,'id desc','');
      if($res === false) return json(['code'=>1,'msg'=>'error:'.$this->error('error:'.$this->service->getError())]);
      return json(['code'=>0,'count'=>$res['total_count'],'data'=>$res['data'],'msg'=>'']);
    }



    $list = [
      ['type'=>'select','title'=>'语言','name'=>'lang','option'=>[['key'=>'全部','value'=>''],['key'=>'中文','value'=>'zh'],['key'=>'英文','value'=>'en']]],
      ['type'=>'s_e_date','title'=>'创建时间','s_name'=>'min_create_time','e_name'=>'max_create_time'],
      ['type'=>'select','title'=>'回复状态','name'=>'status','option'=>[['key'=>'全部','value'=>''],['key'=>'已回复','value'=>'1'],['key'=>'未回复','value'=>'0']]],
    ];
    $this->assign('searchForm',$this->html_template->searchForm($list));
    $this->assign('addButton',$this->html_template->addButton('添加律师','admin','Team','addteam'));
    $this->assign('batheDeleteButton',$this->html_template->batheDeleteButton('table','admin','Team','deleteteam'));
    return $this->fetch();
  }
  /* ------------------------------------------------------------------ */
  /*
  * 添加
  */
  function addTeam(){
        if($this->requestType=='post'){
            $temp=input('post.');
            $temp['create_time']=$temp['update_time']=time();
            $temp['birth']=strtotime($temp['birth']);    
           
            $rel=$this->service->model('Team')->add($temp,'addteam');
            if($rel){
                return $this->success('添加成功');
            }
            return $this->error($this->service->getError());
        }
  	return $this->fetch();
  }
  /* ------------------------------------------------------------------ */
  /*
   * 
  * 编辑
  */
  function editTeam($id=''){
      $data=$this->service->model('Team')->getInfo(['id'=>$id]);
      $this->assign('data',$data);
      if($this->requestType=='post'){
          $temp=input('post.');
          $rel=$this->service->model('Team')->edit($temp,['id'=>$id],'editteam');
          if($rel){
              return $this->success('修改成功');
          }
          return $this->error($this->service->getError());
      }
    return $this->fetch();
  }
  /* ------------------------------------------------------------------ */
  /*
  * 删除
  */
  function deleteTeam($id=''){
    if($this->service->model('Team')->delete($id)){
      return $this->success('删除成功！');
    }
    return $this->error('删除失败！');
  }
 
 
}
