<?php
namespace core\service;
// +----------------------------------------------------------------------
// | 系统配置管理操作
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2017 Even Yin All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Even yin <416467069@qq.com> <13022156261>
// +----------------------------------------------------------------------
use \think\Session as Session;          //用户Session缓存
use \think\Cache   as Cache;            //全局缓存
use core\api\ISystem as ISystem;
use core\service\BaseService as BaseService;
use core\model\System as SystemModel;
// use core\model\SystemAlipayModel as SystemAlipayModel;
// use core\model\SystemSmsModel as SystemSmsModel;
// use core\model\UserActionLogModel as UserActionLogModel;
// use core\model\AddressModel as AddressModel;
// use core\model\SystemCategoryModel as SystemCategoryModel;
// use core\service\User as UserService;

// use core\extend\wechat\WechatConfig as WechatConfig;
class System extends BaseService implements ISystem
{
    protected $system_config;
    protected $system_wechat;
    protected $system_alipay;
    protected $system_sms;
    protected $system_address;
    protected $system_category;

    protected $user_action_log;

    protected $config_system;
    protected $config_wechat;
    protected $config_alipay;
    protected $config_sms;

    /* -----------------------------------START------------------------------- */
    /*
    * 构造函数
    */
    public function __construct(){
      parent::__construct();
      $this->system_config = new SystemModel();
      $this->system_wechat = new WechatConfig();
      $this->system_alipay = new SystemAlipayModel();
      $this->system_sms    = new SystemSmsModel();
      $this->system_address    = new AddressModel();
      $this->user_action_log = new UserActionLogModel();
      $this->system_category = new SystemCategoryModel();

      $this->config_system = Cache::get('system');
      $this->config_wechat = Cache::get('system_wechat');
      $this->config_alipay = Cache::get('system_alipay');
      $this->config_sms = Cache::get('system_sms');

    }
    /* -----------------------------------START------------------------------- */
    /*
    * 获取系统配置
    */
    function getSystem(){
      return $this->system;
    }
    /* -----------------------------------END------------------------------- */
    /*
    * 编辑系统配置
    */
    function editSystem($data=[]){
      $user = new UserService();
      if(isset($data['income'])) unset($data['income']);
      if(isset($data['money'])) unset($data['money']);
      if(isset($data['outlay'])) unset($data['outlay']);
      if(isset($data['member_outlay'])) unset($data['member_outlay']);
      if(isset($data['seller_outlay'])) unset($data['seller_outlay']);
      if(isset($data['service_outlay'])) unset($data['service_outlay']);

      $data['is_member'] = isset($data['is_member'])?$data['is_member']:0;
      $data['is_seller'] = isset($data['is_seller'])?$data['is_seller']:0;
      $data['is_recommender'] = isset($data['is_recommender'])?$data['is_recommender']:0;
      $data['is_account_pay'] = isset($data['is_account_pay'])?$data['is_account_pay']:0;
      $data['is_wechat_pay'] = isset($data['is_wechat_pay'])?$data['is_wechat_pay']:0;
      $data['is_alipay_pay'] = isset($data['is_alipay_pay'])?$data['is_alipay_pay']:0;
      $data['is_close'] = isset($data['is_close'])?$data['is_close']:0;

      if($this->system_config->save($data,['id'=>1])){
        Cache::set('system',null);
        $this->setLog($user->getSessionUid(),1,'修改系统配置SUCCESS');
        return true;
      }
      $this->error = $this->syste_config->getError();
      $this->setLog($user->getSessionUid(),0,'修改系统配置ERROR:'.$this->error);
      return false;
    }
    /* -----------------------------------END------------------------------- */
    /*
    * 获取系统微信配置
    */
    function getSystemWechat(){
      return $this->system_wechat->getConfig();
    }
    /* -----------------------------------END------------------------------- */
    /*
    * 编辑系统微信配置
    */
    function editSystemWechat($data=[]){
      $user = new UserService();
      $data['status'] = isset($data['status'])?$data['status']:0;
      if($this->system_wechat->saveConfig($data)){
        $this->setLog($user->getSessionUid(),1,'修改微信配置SUCCESS');
        return true;
      }
      $this->error = $this->system_wechat->getError();
      $this->setLog($user->getSessionUid(),0,'修改微信配置ERROR:'.$this->error);
      return false;
    }
    /* -----------------------------------END------------------------------- */
    /*
    * 获取系统阿里云配置
    */
    function getSystemALipay(){
      if(empty($this->config_alipay)){
        $this->config_alipay = $this->system_alipay->getInfo(['id'=>1]);
        if($this->config_alipay){
          Cache::set('system_alipay',$this->config_alipay,3600);
          return $this->config_alipay;
        }
        //不存在则保存
        if($this->system_alipay->save(['id'=>1])){
          $this->config_alipay = $this->system_alipay->getInfo(['id'=>1]);
          if($this->config_alipay){
            Cache::set('system_alipay',$this->config_alipay,3600);
            return $this->config_alipay;
          }
        }
      }
      return $this->config_alipay;
    }
    /* -----------------------------------END------------------------------- */
    /*
    * 更新系统阿里云配置
    */
    function editSystemAlipay($data=[]){
      $user = new UserService();
      if($this->system_alipay->save($data,['id'=>1])){
        Cache::set('system_alipay',null);
        $this->setLog($user->getSessionUid(),1,'修改支付宝配置SUCCESS');
        return true;
      }
      $this->error = $this->system_alipay->getError();
      $this->setLog($user->getSessionUid(),0,'修改支付宝配置ERROR:'.$this->error);
      return false;
    }
    /* -----------------------------------END------------------------------- */
    /*
    * 获取系统短信配置
    */
    function getSystemSms(){
      if(empty($this->config_sms)){
        $this->config_sms = $this->system_sms->getInfo(['id'=>1]);
        if($this->config_sms){
          Cache::set('system_sms',$this->config_sms,3600);
          return $this->config_sms;
        }
        if($this->system_sms->save(['id'=>1])){
          $this->config_sms = $this->system_sms->getInfo(['id'=>1]);
          if($this->config_sms){
            Cache::set('system_sms',$this->config_sms,3600);
            return $this->config_sms;
          }
        }
      }
      return $this->config_sms;
    }
    /* -----------------------------------END------------------------------- */
    /*
    * 更新系统短信配置
    */
    function editSystemSms($data=[]){
      $user = new UserService();
      if($this->system_sms->save($data,['id'=>1])){
        Cache::set('system_sms',null);
        $this->setLog($user->getSessionUid(),1,'修改短信配置SUCCESS');
        return true;
      }
      $this->error = $this->system_sms->getError();
      $this->setLog($user->getSessionUid(),0,'修改短信配置ERROR:'.$this->error);
      return false;
    }
    /* -----------------------------------END------------------------------- */
    /*
    * 获取分类列表
    */
    function getCategorys($page=1,$page_size=30,$condition=[],$order='',$field=''){
      return $this->system_category->pageQuery($page, $page_size, $condition, $order, $field);
    }
    /* -----------------------------------END------------------------------- */
    /*
    * 获取一定条件下的分类列表
    */
    function getCategoryByCondition($condition=[],$field='',$order=''){
      return $this->system_category->getQuery($condition, $field, $order);
    }
    /* -----------------------------------END------------------------------- */
    /*
    * 根据ID获取数据
    */
    function getCategoryById($id=''){
      return $this->system_category->getInfo(['id'=>$id]);
    }
    /* -----------------------------------END------------------------------- */
    /*
    * 添加分类
    */
    function addCategory($data=[]){
      if($this->system_category->save($data)){
        return true;
      }
      $this->error = $this->system_category->getError();
      return false;
    }
    /* -----------------------------------END------------------------------- */
    /*
    * 编辑分类
    */
    function editCategory($data=[],$id=''){
      if($this->system_category->save($data,['id'=>$id])){
        return true;
      }
      $this->error = $this->system_category->getError();
      return false;
    }
    /* -----------------------------------END------------------------------- */
    /*
    * 删除分类
    */
    function deleteCategory($id=''){
      return $this->system_category->destroy($id);
    }
    /* -----------------------------------END------------------------------- */
    /*
    * 修改分类单个字段
    */
    function setCategoryField($pk_name,$pk_value,$field_name,$field_value){
      if($pk_name=='' || $pk_value=='' || $field_name=='' || $field_value==''){
        $this->error = '参数错误';
        return false;
      }
      $res = $this->system_category->modifyTableField($pk_name, $pk_value, $field_name, $field_value);
      if(!$res){
        $this->error = $this->system_category->getError();
        return false;
      }
      return true;
    }
    /* -----------------------------------END------------------------------- */
    /*
    * 获取城市列表
    */
    function getCitys($page=1,$page_size=30,$condition=[],$order='',$field=''){
      return $this->system_address->pageQuery($page, $page_size, $condition, $order, $field);
    }
    /* -----------------------------------END------------------------------- */
    /*
    * 获取全部省份
    */
    function getCitysByPid($condition=[],$field='',$order=''){
      return $this->system_address->getQuery($condition, $field, $order);
    }
    /* -----------------------------------END------------------------------- */
    /*
    * 单个修改城市字段信息
    */
    function setCityField($pk_name,$pk_value,$field_name,$field_value){
      if($pk_name=='' || $pk_value=='' || $field_name=='' || $field_value==''){
        $this->error = '参数错误';
        return false;
      }
      $res = $this->system_address->modifyTableField($pk_name, $pk_value, $field_name, $field_value);
      if(!$res){
        $this->error = $this->system_address->getError();
        return false;
      }
      return true;
    }
    /* -----------------------------------END------------------------------- */
    /*
    * 获取日志
    */
    function getLogs($page=1,$page_size=30,$condition=[],$order='',$field=''){
      return $this->user_action_log->pageQuery($page, $page_size, $condition, 'id DESC', $field);
    }
    /* -----------------------------------END------------------------------- */
    /*
    * 删除日志
    */
    function deleteLog($id){
      return $this->user_action_log->destroy($id);
    }
    /* -----------------------------------END------------------------------- */
}
