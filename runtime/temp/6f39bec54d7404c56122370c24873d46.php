<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:59:"D:\wamp\www\dmd\public/../application/admin\view\error.html";i:1508920358;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>error</title>
    <link rel="stylesheet" href="/layui/css/layui.css">
    <style>
    .my-page-box {
      font-family: "Segoe UI", "Lucida Grande", Helvetica, Arial, "Microsoft YaHei", FreeSans, Arimo, "Droid Sans", "wenquanyi micro hei", "Hiragino Sans GB", "Hiragino Sans GB W3", FontAwesome, sans-serif;
      text-align: center;
      padding: 20px;
      background-color: white;
    }

    .my-page-box i {
      font-size: 100px;
    }

    .my-page-box h2, .my-page-box h3, .my-page-box h4, .my-page-box h5 {
      font-size: 80px;
    }

    .my-page-box p.msg {
      /*color: #dce2ec;*/
      font-size: 20px;
      margin-top: 20px;
    }

    .my-page-box p.text {
      color: #666;
      font-size: 16px;
      margin-top: 20px;
      padding:0 20px;
    }

    .my-page-box .my-btn-box {
      margin-top: 20px;
      margin-bottom: 20px;
    }
    </style>
    <script src="/layui/layui.js"></script>
</head>
<body>
  <div class="my-page-box">
      <i class="layui-icon" style="color:#FFB800">&#xe6af;</i>
      <p class="msg"><?php echo(strip_tags($msg));?></p>
      <p class="text">
        友情提示：页面自动 <a id="href" href="<?php echo($url);?>" style="color:#FFB800;">跳转</a> 等待时间： <b id="wait"><?php echo($wait);?></b>
      </p>
  </div>
  <script type="text/javascript">
      (function(){
          var wait = document.getElementById('wait'),
              href = document.getElementById('href').href;
          var interval = setInterval(function(){
              var time = --wait.innerHTML;
              if(time <= 0) {
                  location.href = href;
                  clearInterval(interval);
              };
          }, 1000);
      })();
  </script>
</body>
</html>
