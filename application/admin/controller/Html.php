<?php 
namespace app\admin\controller;
use think\Controller;
use think\View;
class Html extends Controller{
    protected $uid;
    protected $auth;
    protected $view;
    
    /*构造函数*/
    public function __construct($data=[],$uid='',$auth=''){
        $this->uid=$uid;
        $this->auth=$auth;
        $this->view=new view;
        
    }
    /*搜索表单*/
    function searchForm($list=[]){
        if(empty($list)) return null;
        return $this->view->fetch('html/searchForm',['list'=>$list]);
    }
    
    /*编辑按钮js代码*/
    function statusScript($url=''){
        if($url='') return null;
        return $this->view->fetch('html/editScript',['url'=>$url]);
    }
    /*打开新链接窗口*/
    function openFrameSript($name='',$title='',$type=2,$id='',$url=''){
        if($name=''||$title=''||$id=''||$url='')
                 return null;
        return $this->view->fetch('html/openFrameScript',['name'=>$name,'title'=>$title,'type'=>$type,'id'=>$id,'url'=>$url]);
            
        
    }
    /*编辑按钮js代码*/
    function editFieldScript($name='',$title='',$type=0,$url=''){
        if($url==='') return null;
        return $this->view->fetch('html/editFieldScript',['name'=>$name,'title'=>$title,'type'=>$type,'url'=>$url]);
        
    }
    /*编辑js按钮代码*/
    function editTextAreaScript($url=''){
        if($url='') return null;
        return $this->view->fetch('html/editScript',['url'=>$url]);
    }
    /*删除按钮js代码*/
    function deleteScript($url=''){
        if($url='') return null;
        return $this->view->fetch('html/deleteScript',['url'=>$url]);
    }
    /*添加按钮*/
    function addButton($title='',$module='',$controller='',$action=''){
        return $this->view->fetch('html/addButton',['title'=>$title,'url'=>url($module.'/'.$controller.'/'.$action)]);
        
    }

    /*批量删除*/
    function batheDeleteButton($tablename='',$module='',$controller='',$action=''){
        return $this->view->fetch('html/batheDeleteButton',['tablename'=>$tablename,'url'=>url($module.'/'.$controller.'/'.$action)]);
        
    }
    
}
