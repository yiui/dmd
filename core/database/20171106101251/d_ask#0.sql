DROP TABLE IF EXISTS `d_ask`;
CREATE TABLE `d_ask` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(21) DEFAULT NULL,
  `mobile` varchar(21) NOT NULL,
  `content` text NOT NULL,
  `create_time` int(11) NOT NULL,
  `status` int(1) DEFAULT NULL,
  `lang` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='咨询';
insert into `d_ask` VALUES  ( '1','杰克','1832222222222','22222222222','1509673126','','1' ), ( '2','张三1','14311111111','内容1','1509673573','1','1' ) ;
