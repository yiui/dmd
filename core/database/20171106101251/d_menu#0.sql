DROP TABLE IF EXISTS `d_menu`;
CREATE TABLE `d_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL,
  `url` varchar(50) DEFAULT NULL,
  `description` text,
  `status` int(1) DEFAULT '1' COMMENT '0表示不可用，1表示可用',
  `create_time` char(10) DEFAULT '',
  `is_link` int(1) DEFAULT NULL,
  `is_menu` int(1) DEFAULT NULL,
  `lang` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COMMENT='菜单表';
insert into `d_menu` VALUES  ( '1','新闻','www.baidu.com','百度网站','1','1','11','1','1' ), ( '3','标题2222','http://www.baidu.com','miaoshu','1','1509335437','0','0','2' ), ( '4','新浪3333','http://sina.com','描述333','0','1509335644','0','0','2' ), ( '5','1234567','http://www.bd.com','11','1','1509411376','0','0','1' ), ( '6','1234567','http://www.baidu.com','描述','1','1509412135','0','0','1' ), ( '7','12345678','http://www.baidu.com','描述','1','1509412601','0','0','1' ), ( '8','123456','http://www.baidu.com','111','1','1509413756','0','0','1' ), ( '9','12343','http://www.baidu.com','这是一个关','1','1509413851','0','0','1' ), ( '10','1111111','','11','1','1509532020','','','1' ), ( '11','12345','http://www.baidu.com','11','1','1509585573','','','1' ), ( '12','12345','','','1','1509585652','','','1' ), ( '13','12345','','','1','1509585921','','','1' ), ( '14','11','http://www.baidu.com','11','1','1509593698','','','1' ), ( '15','111','http://www.baidu.com','描述','1','1509593815','','','1' ), ( '16','11','http://www.baidu.com','ms ','1','1509593998','','','1' ) ;
