<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:68:"D:\wamp\www\dmd\public/../application/admin\view\html\addButton.html";i:1508919739;}*/ ?>
<button class="layui-btn layui-btn-primary" id="add"><?php echo $title; ?></button>
<script>
  layui.use(['jquery'], function(){
    var $ = layui.jquery;
    $('#add').on('click',function(){
      layer.open({
          title:'<?php echo $title; ?>',
          type: 2,
          maxmin: true,
          area: ['<?php echo request()->isMobile()?'100%':'500px'; ?>', '420px'],
          id:'addButton',
          content: '<?php echo $url; ?>',
          end: function(){
            location.href = location;
          }
      });
    });
  });
</script>
