<?php
namespace core\model;
// +----------------------------------------------------------------------
// | 公司表
// +----------------------------------------------------------------------
class Company extends BaseModel
{
    protected $table      = 'company';
    protected $rule       = [
        
    ];
    protected $scene_rule = [
        'name' => 'require|unique:Company|max:50',
        'mobile'=>'require|min:7|max:20|number',
        'addr'=>'require|max:100',
        'description'=>'require'
    ];
    protected $msg        = [
        'name.require'=>'公司名不能为空',
        'name.unique'=>'该公司已经被添加过了',
        'name.max'=>'公司名称不能超过50个字符',
        'mobile.require'=>'电话不能为空',
        'mobile.min'=>'电话不能少于7个数字',
        'mobile.max'=>'电话不能超过20个数字',
        'mobile.number'=>'电话必须为数字',
        'addr.require'=>'地址不能为空',
        'addr.max'=>'地址不能超过100个字符',
        'description.require'=>'描述必须',
        
    ];
    protected $scene      = [
        'addindex' => ['name','mobile', 'addr','description'],
        'editindex' => ['name.unique','name.max','mobile','addr','description'],
       
       
    ];
    
}
