<?php
namespace core\service;
// +----------------------------------------------------------------------
// | 会员服务层
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2017 Even Yin All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Even yin <416467069@qq.com> <13022156261>
// +----------------------------------------------------------------------
use core\api\IMember       as IMember;
use core\model\MemberModel as MemberModel;
use think\Session          as Session;
class Member extends MemberBaseService implements IMember
{
  protected $member_model;
  /* -----------------------------------START------------------------------- */
  /*
  * 构造函数
  */
  function __construct()
  {
    parent::__construct();
    $this->member_model = new MemberModel();
  }
  /* -----------------------------------START------------------------------- */
  /*
  * 获取会员id
  */
  function getMemberId(){
    return Session::get('member.id');
  }
  /* -----------------------------------END------------------------------- */
  /*
  * 获取会员资料
  */
  function getSessionMemberProfile(){
    return Session::get('member');
  }
  /* -----------------------------------END------------------------------- */
  /*
  * 获取会员列表
  */
  /* -----------------------------------END------------------------------- */
  /*
  * 获取会员资料
  */
  function getMemberProfile($mid=''){
    return $this->member_model->getInfo(['id'=>$mid]);
  }
  /* -----------------------------------END------------------------------- */
  /*
  * 会员登录
  */
  function login($username='',$password=''){
    $member = $this->member_model->getInfo(['username|mobile'=>$username]);
    if(empty($member) || empty($member->sign) || empty($member->password) || md5(md5($password).$member->sign)!=$member->password){
      $this->error = '账号或密码错误';
      return false;
    }
    unset($member['sign']);
    unset($member['password']);
    Session::set('mid',$member['id']);
    Session::set('member',$member);
    return true;
  }
  /* -----------------------------------END------------------------------- */
  /*
  * 会员手机号登录
  */
  function mobileLogin($mobile='',$code=''){
    if(!$this->checkMobileCode($mobile,$code)) return false;
    $member = $this->member_model->getInfo(['mobile'=>$mobile]);
    if(empty($member)){
      $this->error = '用户尚未注册！';
      return false;
    }
    unset($member['sign']);
    unset($member['password']);
    Session::set('mid',$member['id']);
    Session::set('member',$member);
    return true;
  }
  /* -----------------------------------END------------------------------- */
  /*
  * 会员微信登录
  */
  function wechatLogin($user=[]){
    if(empty($user)){
      $this->error = '用户微信信息获取失败！';
      return false;
    }
    $member = $this->member_model->getInfo(['openid'=>$user['openid']]);
    if(empty($member)){
      $this->error = '用户尚未注册！';
      return false;
    }
    unset($member['sign']);
    unset($member['password']);
    Session::set('mid',$member['id']);
    Session::set('member',$member);
    return true;
  }
  /* -----------------------------------END------------------------------- */
  /*
  * 会员注册
  * @param bigInt $mobile
  * @param string $password
  * @param string $code
  * @param string $recommender_code 推荐码
  * @param string $recommender_id 推荐人id
  */
  function register($data=[]){
    $data['username'] = $data['mobile'];
    $data['create_time'] = $data['update_time'] = strtotime(date('YmdHis'));
    $data['recommender_code'] = $this->getRandomStr(6);
    while ($this->member_model->getCount(['recommender_code'=>$data['recommender_code']]) > 0) {
      $data['recommender_code'] = $this->getRandomStr(6);
    }
    if($this->member_model->save($data,[],'register')){
      $member = $this->member_model->getInfo(['mobile'=>$data['mobile']]);
      Session::clear();
      if(empty($member)){
        $this->error = '用户信息获取失败！';
        return false;
      }
      Session::set('mid',$member['id']);
      Session::set('member',$member['member']);
      return true;
    }
    $this->error = $this->member_model->getError();
    return false;
  }
  /* -----------------------------------END------------------------------- */
  /*
  * 会员退出
  */
  function logout(){
    Session::clear();
    return true;
  }


  /* -----------------------------------END------------------------------- */
  /*
  * 修改会员资料
  */
  function changeProfile($member_id='',$nickname='',$headimgurl='',$sex='',$province='',$city='',$district='',$street=''){

  }
  /* -----------------------------------END------------------------------- */
  /*
  * 修改密码
  */
  function changePwd($member_id='',$old_password='',$new_password=''){

  }
  /* -----------------------------------END------------------------------- */
  /*
  * 找回密码
  */
  function findPwd($mobile='',$new_password='',$code=''){

  }
  /* -----------------------------------END------------------------------- */
  /*
  * 找回手机号
  */
  function findMobile($mobile=''){
    $member = $this->member->where('mobile',$mobile)->find();
    return $member;
  }

}
