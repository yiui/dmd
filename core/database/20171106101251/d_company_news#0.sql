DROP TABLE IF EXISTS `d_company_news`;
CREATE TABLE `d_company_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `cover` varchar(100) NOT NULL,
  `description` text,
  `content` text NOT NULL,
  `edit_time` int(11) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `create_time` int(11) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `lang` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='公司i信息表';
insert into `d_company_news` VALUES  ( '1','万科前三季度赚了111亿 销售额已超去年全年','uploads1201710298794f1b6f75d08274822c59701bccf6d.jpg','万科前三季度赚了111亿 销售额已超去年全年','<p>万科(000002)10月26日晚间公布2017年第三季度报告。前三季度,万科累计实现营业收入1171亿元,实现归属于上市公司股东的净利润110.9亿元,同比增长34.2%。</p><p>　　今年7-9月,万科实现销售面积795.5万平方米,销售金额1189.2亿元,同比分别增长36.5%和63.3%。前三季度,万科累计实现销售面积2664.5万平方米,销售金额3961亿元,同比分别增长33.8%和50.7%,销售金额超过2016年全年水平。</p><p>　　下半年以来,全国商品住房销售进一步放缓。国家统计局数据显示,第三季度全国商品住宅销售面积同比下降1.7%,为2015年第二季度以来首次同比下跌,商品住宅销售金额同比仅增长1.3%。与此同时,部分省市继续加大调控力度,推出包括限售在内的各项政策以稳定市场,热点城市的新房供应与成交面积持续回落。</p>','1509280717','1','1509090147','3','1' ), ( '3','Returns an SQLSTATE, ','uploads1201710278dd5fd01aa1ee4a6893c53a5708b8c25.jpg','Returns an SQLSTATE, ','<p>Returns an SQLSTATE, Returns an SQLSTATE, Returns an SQLSTATE, Returns an SQLSTATE, Returns an SQLSTATE, Returns an SQLSTATE, Returns an SQLSTATE, Returns an SQLSTATE, Returns an SQLSTATE, Returns an SQLSTATE,&nbsp;</p>','0','1','1509096467','3','2' ), ( '4','标题111','uploads1201710307cfd87d2d003fafff8bd9d431c4b11ce.jpg','描述111','','0','1','1509320750','3','1' ), ( '5','111111','\uploads\1\20171102\3a042b75d1c87a59a0e4c49640f35581.jpg','描述','','1509670400','1','1509589981','3','1' ), ( '6','11111111','\uploads\1\20171102\4857d211ccdd21d7c1c32c730332579c.jpg','11','','1509670263','1','1509592690','3','1' ), ( '7','标题1111','\uploads\1\20171102\69a7ce66f5b26b469af4ece1bc877440.jpg','描述1','33','1509801299','0','1509612965','3','1' ) ;
