<?php
namespace core\model;
// +----------------------------------------------------------------------
// | 公司信息管理
// +----------------------------------------------------------------------
class CompanyNews extends BaseModel
{
    protected $table      = 'company_news';
    protected $rule       = [];
    protected $scene_rule = [
        'title'=>'require|min:3',
        'cover'=>'require',
//        'description'=>'require',
//        'content'=>'require'
    ];
    
    protected $msg        = [
        'title.require'=>'标题不能为空',
        'title.min'=>'标题不能超过3个字符',
        'cover.require'=>'图片不能为空',
//        'description.require'=>'描述内容不能为空',
//        'content.require'=>'请填写描述内容'
    ];
    
    protected $scene      = [
        'addnews'=>['title','cover'],
        
    ];
    
  protected function getCompanyIdAttr($value){
      $rel=db('company')->where('id',$value)->find();
      if($rel){
          return $rel['name'];
      }
      return null;
  }
    
}
