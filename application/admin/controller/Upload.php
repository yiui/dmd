<?php
namespace app\admin\controller;
// +----------------------------------------------------------------------
// | 上传管理模块
// +----------------------------------------------------------------------

use core\service\Upload as UploadService;
class Upload extends BaseController
{
  protected $upload;
  /* -----------------------------------START------------------------------- */
  /*
  * 初始化
  */
  protected function __init(){
    parent::__init();
    $this->upload = new UploadService();
  }
  /* -----------------------------------END------------------------------- */
  /*
  * 用户拥有上传操作全部权限
  */
  protected function __auth(){}
  /* -----------------------------------END------------------------------- */
  /*
  * 上传本地图片
  */
  function image(){
    $file = request()->file('file');
    $src = DS.'uploads'.DS.$this->uid;
    $path = ROOT_PATH.'public'.$src;
    if($this->upload->image($src,$path,$file)){
      // $this->service->setLog($this->uid,1,'图片上传SUCCESS');
      return json(['code'=>1,'src'=>$this->upload->getSrc()]);
    }
    // $this->service->setLog($this->uid,0,'图片上传ERROR:'.$this->getError());
    return json(['code'=>0,'msg'=>$this->upload->getError()]);
  }
  /* ------------------------------------------------------------------ */
  /*
  * 上传本地图片
  */
  function articleImage(){
    $file = request()->file('file');
    $src = DS.'uploads'.DS.$this->uid;
    $path = ROOT_PATH.'public'.$src;
    if($this->upload->image($src,$path,$file)){
      return json(['code'=>0,'msg'=>'','data'=>['src'=>request()->domain().$this->upload->getSrc(),'title'=>$this->system['name']]]);
    }
    return json(['code'=>1,'msg'=>$this->upload->getError()]);
  }
  /* ------------------------------------------------------------------ */
  /*
  * 上传base64图片
  */
  function base64image(){

  }
  /* ------------------------------------------------------------------ */
  /*
  * 上传视频到OSS
  */
  function videoToOss(){

  }
  /* -----------------------------------END------------------------------- */
  /*
  * 上传其它文件
  */
  function file(){
		$file = request()->file('file');
    $src = DS.'uploads'.DS.$this->uid;
    $path = ROOT_PATH.'public'.$src;
    if($this->upload->file($src,$path,$file)){
      switch ($this->upload->getExtension()) {
        case 'rar':
            $cover = '/icon/rar.png';
            break;
        case 'zip':
            $cover = '/icon/zip.png';
            break;
        case 'doc':
            $cover = '/icon/doc.png';
          break;
        case 'txt':
            $cover = '/icon/txt.png';
            break;
        case 'xls':
            $cover = '/icon/excel.png';
          break;
        case 'avi':
        case 'mp4':
            $cover = '/icon/video.png';
          break;
        case 'mp3':
            $cover = '/icon/music.png';
          break;
        case 'pdf':
            $cover = '/icon/pdf.png';
          break;
        case 'bmp':
        case 'gif':
        case 'jpeg':
        case 'png':
        case 'jpg':
            $cover = '/icon/image.png';
          break;
        default:
            $cover = '/icon/file.png';
          break;
      }
      return json(['code'=>1,'cover'=>$cover,'src'=>$this->upload->getSrc()]);
    }
    return json(['code'=>0,'msg'=>$this->upload->getError()]);
  }
  /* -----------------------------------END------------------------------- */
}
