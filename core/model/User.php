<?php
namespace core\model;
// +----------------------------------------------------------------------
// | 用户
// +----------------------------------------------------------------------

class User extends BaseModel
{
    protected $table      = 'user';
    protected $rule       = [];
    protected $scene_rule = [
        'username'=>'require|max:20|unique:User',
        'password'=>'require|max32',
        'name'=>'require|max:20',
        'mobile'=>'require|number|max:20',
        'birth'=>'require|max:11',
        'description'=>'require',
    ];
    protected $msg        = [
        'username.require'=>'用户名必须',
        'username.max'=>'用户名不超过20个字符',
        'username.unique'=>'用户名必须唯一',
        'password.require'=>'密码必须',
        'name.require'=>'姓名必须',
        'name.max'=>'姓名不超过20个字符',
        'password.max'=>'密码不超过32个字符',
        'mobile.require'=>'电话必须',
        'mobile.number'=>'电话号码必须为数字',
        'mobile.max'=>'电话号码不超过20个数字',
        'birth.require'=>'生日必须',
        'birth.max'=>'生日不超过11个字符',
       
        
    ];
    protected $scene      = [
        'addusers'=>['useranme','password','mobile','birth'],
        'editusers'=>['useranme','mobile','birth'],
    ];
    
//    public  function getBirthAttr($value){
//        return date('Y-m-d',$value);
//    }
}
