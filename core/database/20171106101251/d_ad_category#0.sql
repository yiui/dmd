DROP TABLE IF EXISTS `d_ad_category`;
CREATE TABLE `d_ad_category` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` text NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `lang` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='广告分类';
insert into `d_ad_category` VALUES  ( '1','banner','描述','1','1' ), ( '2','广告图','123456','1','1' ) ;
