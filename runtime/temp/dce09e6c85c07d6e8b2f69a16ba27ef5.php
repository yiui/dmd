<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:67:"D:\wamp\www\dmd\public/../application/admin\view\team\editteam.html";i:1509761354;s:60:"D:\wamp\www\dmd\public/../application/admin\view\layout.html";i:1509011174;}*/ ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width,initial-scale=1, maximum-scale=1, user-scalable=no">
  <title>管理后台|江苏大名大律师事务所</title>
  <link rel="stylesheet" href="__PUBLIC__/layui/css/layui.css">
  <script src="__PUBLIC__/layui/layui.js"></script>
  <style>
    body{background-color: #eee;min-width: 1200px;}
    .container{margin:0 auto;width: 1200px;}
    .head{height: 80px;background-color: #fff;border-bottom: 1px solid #ddd;border-top:5px solid #444;}
    .head .logo{#float:left;background-image: url(http://dmd.qianbeinet.com/dmd_logo.png);background-size: cover;width: 200px;height:40px;margin-top: 20px;float: left;}
    .head .layui-nav{background-color:#fff;border-radius:0;float: right;padding:18px 0;}
    .head .layui-nav .layui-nav-more{display: none}
    .head .layui-nav-child{top:62px;box-shadow:none;}
    .head .layui-nav .layui-nav-item{line-height: 24px;text-align: center;}
    .head .layui-nav .layui-nav-item p{font-size: 12px;}
    .head .layui-nav .layui-nav-item a{color: #7A7A7A;font-size: 13px;padding:10px 36px;}
    .head .layui-nav .layui-nav-item a:hover,.head .layui-nav .layui-this a{color: #fff;background-color: #444;font-size: 13px;}
    .head .layui-nav .layui-this:after, .layui-nav-bar, .layui-nav-tree .layui-nav-itemed:after{height:0;}

    .footer{background-color: #eee;color:rgba(0, 0, 0, 0.5);font:14px Helvetica Neue,Helvetica,PingFang SC,\5FAE\8F6F\96C5\9ED1,Tahoma,Arial,sans-serif;}
    .footer a{color:rgba(255,255,255,.6);line-height: 24px;font-size: 13px;color:rgba(255,255,255,.3);}
    .footer .footer-link-block{min-height: 200px;}
    .footer .footer-link-title{font-size: 16px;color: rgba(255,255,255,.8);margin-bottom: 10px;}

    .layui-btn{background-color: #444;}
    .layui-btn-normal{background-color: #1E9FFF;}
    .layui-btn-danger{background-color: #FF5722;}
    .layui-btn-primary{background-color: #fff;}
    .layui-btn-primary:hover{border-color:#444;}
  </style>
  <style media="screen">
    .teacher{min-height:600px;background-color:#fff;border:1px solid #ddd;}
    .layui-nav-tree .layui-nav-item a:hover{}
      .layui-nav-tree .layui-nav-bar{width: 0}
    .layui-nav-tree .layui-nav-item{line-height: 48px}
    .layui-nav-tree .layui-nav-item a{height: 48px;color: #000;text-align: center;}
    .layui-nav-tree .layui-nav-item a:hover{background-color:#444;}
    .layui-nav-tree.layui-nav{background-color: #fff}
    .layui-nav-tree .layui-nav-child dd.layui-this,
     .layui-nav-tree .layui-nav-child dd.layui-this a,
      .layui-nav-tree .layui-this,
       .layui-nav-tree .layui-this>a,
       .layui-nav-tree .layui-this>a:hover{background-color: #444;}
    .layui-nav-tree .layui-this>a{color: #fff;}

    .teacher .layui-tab-card{border: 0;box-shadow: none;border-radius: 0}
    .teacher .layui-tab{margin: 0}
    .teacher .layui-tab .layui-tab-title{height: 48px;background-color: #fff;}
    .teacher .layui-tab .layui-tab-title li{line-height: 48px;padding:0 65px;}
    .teacher .layui-tab .layui-tab-title li.layui-this a{color: #fff;}
    .teacher .layui-tab .layui-tab-title .layui-this{background-color: #444;color: #fff}
    .teacher .layui-tab .layui-tab-title .layui-this:after{height: 48px;border: 0;}
    .teacher .layui-tab .layui-tab-content{padding: 20px}
    .teacher .layui-tab .layui-tab-title .layui-tab-bar{height: 48px;line-height: 48px;}

    .layui-form-item{margin-bottom: 30px;}
    input:disabled{background-color: #eee}
    .layui-form-select dl dd.layui-this{background-color: #444;}
    .layui-laydate .layui-this{background-color: #444!important;}
    .layui-laypage .layui-laypage-curr .layui-laypage-em{background-color: #444;}
  </style>
  
<style>
body{min-width: inherit;background-color: #fff;overflow-x: hidden;}
.teacher{display: none;}
.layui-layout-admin .layui-body{top:10px;}
form{padding:50px 0;}
.layui-footer{width: 100%;position: fixed;bottom: 0;padding:10px 0px;background-color: #eee;text-align: center;}
</style>

</head>

<body>
  
  
<form class="layui-form" action="" method="post">
 
  
  <div class="layui-form-item">
    <div class="layui-inline">
      <label class="layui-form-label">姓名</label>
      <div class="layui-input-inline">
        <input type="text" disabled  value='<?php echo $data['name']; ?>' placeholder="" required lay-verify="name" autocomplete="off" class="layui-input">
      </div>
    </div>
  </div>
    
    <div class="layui-form-item">
    <div class="layui-inline">
      <label class="layui-form-label">手机</label>
      <div class="layui-input-inline">
        <input type="text" name="mobile" value='<?php echo $data['mobile']; ?>' placeholder="" required lay-verify="phone" autocomplete="off" class="layui-input">
      </div>
    </div>
  </div>
    
     <div class="layui-form-item" pane="">
    <label class="layui-form-label">性别</label>
    <div class="layui-input-block">
      <input type="radio" name="sex" value="1" title="男" <?php if($data['sex'] == '1'): ?>checked<?php endif; ?>>
      <input type="radio" name="sex" value="0" title="女" <?php if($data['sex'] == '0'): ?>checked<?php endif; ?>>
    </div>
  </div>
    
    
     <div class="layui-form-item">
    <div class="layui-inline">
      <label class="layui-form-label">生日</label>
      <div class="layui-input-inline">
        <input type="text" name="birth" value="<?php echo date('Y-m-d',$data['birth']); ?>" id="date" lay-verify="date" placeholder="yyyy-MM-dd" autocomplete="off" class="layui-input">
      </div>
    </div>
          </div>
    
     <div class="layui-form-item">
    <div class="layui-inline">
      <label class="layui-form-label">头像</label>
      <div class="layui-upload layui-input-inline">
        <button type="button" class="layui-btn layui-btn-primary" id="cover">上传图片</button>
        <input class="layui-upload-file" type="file" name="cover">
        <div class="layui-upload-list">
          <img class="layui-upload-img" src="__ROOT__/<?php echo !empty($data['cover'])?$data['cover']:'/static/image/upload.png'; ?>" id="cover-img" width="120" height="68">
          <input type="hidden" name="cover" id="cover-val" value="<?php echo !empty($data['cover'])?$data['cover']:''; ?>" lay-verify="cover" autocomplete="off" class="layui-input">
        </div>
        <div id="logo-err"></div>
      </div>
      <script>
        layui.use(['jquery','upload'],function(){
          var $ = layui.jquery;
          var upload = layui.upload;
          var uploadInst = upload.render({
            elem: '#cover',
            url: '<?php echo url('upload/image'); ?>',
            size:2*1024,
            before: function(obj){

            },
            done: function(res){
              if(res.code == 1){
                $('#cover-img').attr('src', res.src);
                $('#cover-img').css('display','block');
                $('#cover-val').val(res.src);
                return true;
              }
            },
            error: function(){
              var errText = $('#cover-err');
              errText.html('<span style="color: #FF5722;">上传失败</span> <a class="layui-btn layui-btn-mini logo-reload">重试</a>');
              errText.find('.cover-reload').on('click', function(){
              	errText.remove();
                uploadInst.upload();
              });
            }
          });
        });
      </script>
    </div>
  </div>
    
     <div class="layui-form-item">
    <div class="layui-block">
      <label class="layui-form-label">描述</label>
      <div class="layui-input-block" style="padding-right:20px;">
        <textarea type="text" name="description" placeholder="" required lay-verify="description" class="layui-textarea"><?php echo $data['description']; ?></textarea>
      </div>
    </div>
  </div>




  <div class="layui-form-item">
    <div class="layui-block">
      <label class="layui-form-label">内容</label>
      <div class="layui-input-block" style="padding-right:20px;">
        <textarea type="text" name="content" placeholder="" required lay-verify="content" class="layui-textarea"><?php echo $data['content']; ?></textarea>
      </div>
    </div>
  </div>

    
    
  <div class="layui-form-item">
    <div class="layui-inline">
      <label class="layui-form-label">回复状态</label>
      <div class="layui-input-inline">
        <select name="status" lay-filter="">
          <option value="0" <?php if($data['status'] == '0'): ?>selected<?php endif; ?>>未回复</option>
          <option value="1"  <?php if($data['status'] == '1'): ?>selected<?php endif; ?>>已回复</option>
        </select>
      </div>
    </div>
  </div>

  <div class="layui-footer">
    <button class="layui-btn layui-btn-normal layui-btn-small" lay-submit lay-filter="*" type="button">立即提交</button>
    <button class="layui-btn layui-btn-small layui-btn-primary" id="closeFrame" type="button">完成/取消</button>
  </div>

</form>
<script>
  layui.use(['layer','jquery','form','laydate'],function(){
    var $ = layui.jquery;
    var form = layui.form;
    var layedit = layui.layedit;
    var laydate=layui.laydate;

   laydate.render({
       elem:'#date'
   });


    form.verify({
      name:function(value){if(value.length < 1){return '姓名不能为空';}},
     cover:function(value){if(value.length < 1){return '请选择要上传的图片';}},
     description:function(value){if(value.length < 1){return '描述不能为空';}},
        content:function(value){if(value.length < 1){return '内容不能为空';}}
    });

    //监听提交
    form.on('submit(*)', function(data){
      data.form.submit();
      return true;
    });
    $('#closeFrame').on('click',function(){
      parent.layer.closeAll();
    })
    $('#addData').on('click',function(){
      $('form').submit();
    })
  });
</script>

  <div class="container">
    <div class="layui-row layui-col-space10" style="margin-top:5px;">
      <div class="layui-col-xs2">
        <div class="teacher">
          
        </div>
      </div>
      <div class="layui-col-xs10">
        <div class="teacher">
          
        </div>
      </div>
    </div>
  </div>
  


  <script>
      layui.use(['element'],function(){
        var element = layui.element;
      });
  </script>
  

</body>
</html>
