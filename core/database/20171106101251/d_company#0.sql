DROP TABLE IF EXISTS `d_company`;
CREATE TABLE `d_company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `addr` varchar(100) DEFAULT NULL,
  `description` text,
  `status` tinyint(1) DEFAULT '1',
  `create_time` int(11) DEFAULT NULL,
  `edit_time` int(11) DEFAULT NULL,
  `lang` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='公司表';
insert into `d_company` VALUES  ( '3','万科企业股份有限公司','4000033333333','深圳','万科企业股份有限公司','1','1509088230','0','1' ), ( '4','中国南玻集团股份有限公司','400233333333','深圳','中国南玻集团股份有限公司科技公司 ','1','1509088272','1509088599','1' ), ( '5','康佳集团股份有限公司','400002222222','深圳','康佳集团股份有限公司','0','1509088316','1509698283','2' ) ;
