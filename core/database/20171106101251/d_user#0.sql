DROP TABLE IF EXISTS `d_user`;
CREATE TABLE `d_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` char(32) NOT NULL,
  `name` varchar(20) NOT NULL COMMENT '真实姓名',
  `mobile` varchar(20) DEFAULT NULL,
  `birth` int(11) DEFAULT NULL,
  `description` text,
  `create_time` int(11) DEFAULT NULL,
  `status` int(1) DEFAULT '0',
  `update_time` int(11) DEFAULT NULL,
  `power` int(1) NOT NULL DEFAULT '1',
  `is_able` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
insert into `d_user` VALUES  ( '1','admin','e10adc3949ba59abbe56e057f20f883e','jack','19811111111','1506960000','这是一个超级用户','1483203661','1','1509021280','1','1' ), ( '2','roses','e10adc3949ba59abbe56e057f20f883e','rose','19822222222','1263225600','','1509075892','1','1509419054','1','1' ), ( '3','zs22222','c33367701511b4f6020ec61ded352059','张三','18722222223','1168963200','','1509076061','1','1509616477','1','1' ), ( '4','111111111111','e10adc3949ba59abbe56e057f20f883e','jack','12345678901','1506787200','','1509076955','1','1509617034','1','1' ), ( '5','amintor','c33367701511b4f6020ec61ded352059','张三','19812345678','1506873600','','1509414714','1','1509671331','1','1' ) ;
