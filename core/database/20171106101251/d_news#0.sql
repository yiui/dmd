DROP TABLE IF EXISTS `d_news`;
CREATE TABLE `d_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL,
  `cover` varchar(100) DEFAULT NULL,
  `description` text,
  `content` text,
  `create_time` int(11) DEFAULT NULL,
  `edit_time` int(11) DEFAULT NULL,
  `is_public` int(1) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `lang` int(1) DEFAULT '1',
  `cid` int(2) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`title`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='新闻表';
insert into `d_news` VALUES  ( '2','标题111','\uploads\1\20171102\ee103a4f53fe6f94c1d99dea2176d142.jpg','描述1','内容1','1509615920','','','1','1','3' ), ( '3','标题2','\uploads\1\20171102\23406bab6654385447603900a24e3926.jpg','描述2','','1509616537','','','1','1','5' ), ( '4','111','\uploads\1\20171103\4164fb18afc7dc4f338339b7f8b65b69.jpg','11','11','1509677568','','','1','1','3' ) ;
