<?php

namespace app\admin\controller;

// +----------------------------------------------------------------------
// | 新闻管理
// +----------------------------------------------------------------------

class News extends BaseController {
    /* -----------------------------------END------------------------------- */
    /*
     * 新闻管理
     */

    function index($page = 1, $limit = 30, $lang = '', $id='',$title = '', $cid = '', $title = '', $create_time = '', $min_create_time = '', $max_create_time = '', $status = '') {
        if ($this->requestType === 'post') {
            $condition = [];
            if($id!==''){
                $condition['id']=$id;
            }
            if ($lang !== '') {
                $condition['lang'] = $lang;
            }
            if ($cid !== '') {
                $condition['cid'] = $cid;
            }
            if ($title !== '') {
                $condition['title'] = $title;
            }
            if ($create_time !== '') {
                $condition['create_time'] = $create_time;
            }
            if ($min_create_time !== '') {
                $condition['create_time'] = ['>=', strtotime($min_create_time)];
            }
            if ($max_create_time !== '') {
                $condition['create_time'] = ['<=', strtotime($max_create_time . ' 23:59:59')];
            }
            if ($status !== '') {
                $condition['status'] = $status;
            }
            $res = $this->service->model('News')->getPageList($page, $limit, $condition, 'id desc', '');
            if ($res === false)
                return json(['code' => 1, 'msg' => 'error:' . $this->error('error:' . $this->service->getError())]);
            return json(['code' => 0, 'count' => $res['total_count'], 'data' => $res['data'], 'msg' => '']);
        }



        $list = [
                ['type' => 'select', 'title' => '语言', 'name' => 'lang', 'option' => [['key' => '全部', 'value' => ''], ['key' => '中文', 'value' => '1'], ['key' => '英文', 'value' => '2']]],
                ['type' => 's_e_date', 'title' => '创建时间', 's_name' => 'min_create_time', 'e_name' => 'max_create_time'],
                ['type' => 'select', 'title' => '发布状态', 'name' => 'status', 'option' => [['key' => '全部', 'value' => ''], ['key' => '已发布', 'value' => '1'], ['key' => '未发布', 'value' => '0']]],
        ];
        $this->assign('searchForm', $this->html_template->searchForm($list));
        $this->assign('addButton', $this->html_template->addButton('添加文章', 'admin', 'news', 'addNews'));
        $this->assign('batheDeleteButton', $this->html_template->batheDeleteButton('table', 'admin', 'news', 'deleteNews'));
        $this->assign('news',$this->service->model('News')->getList(['status'=>1]));
        return $this->fetch();
    }

    /* ------------------------------------------------------------------ */
    /*
     * 添加新闻
     */

    function addNews() {
        if ($this->requestType === 'post') {
            $data = input('post.');
            $data['create_time'] = time();
            $data['content'] = htmlspecialchars($data['content']);
           
            if ($this->service->model('News')->add($data, 'addnews')) {
                return $this->success('添加成功！');
            }
            return $this->error($this->service->getError());
        }
        $this->assign('langs', $this->service->model('Lang')->getList(['status' => 1]));
        $this->assign('categorys', $this->service->model('NewsCategory')->getList(['status' => 1]));
        return $this->fetch();
    }

    /* ------------------------------------------------------------------ */
    /*
     * 
     * 编辑新闻
     */
    function editNews($id = '') {
        $news = $this->service->model('News')->getInfo(['id' => $id]);
        if ($this->requestType === 'post') {
            $data = input('post.');
            $data['content'] = htmlspecialchars($data['content']);
            $data['update_time'] = time();
            if($data['title']==$news['title']){
                unset($data['title']);
            } else if(empty($data['title'])){
                return $this->error('标题不能为空');
            }
            if ($this->service->model('News')->edit($data, ['id' => $id],'editnews')){
                return $this->success('修改成功！');
            }
            return $this->error($this->service->getError());
        }
        $this->assign('news', $news);
        $this->assign('langs', $this->service->model('Lang')->getList(['status' => 1]));
        $this->assign('categorys', $this->service->model('NewsCategory')->getList(['status' => 1]));
        return $this->fetch();
    }

    /* ------------------------------------------------------------------ */
    /*
     * 删除新闻
     */

    function deleteNews($id = '') {
        if ($this->service->model('News')->delete($id)) {
            return $this->success('删除成功！');
        }
        return $this->error('删除失败！');
    }

    /* ------------------------------------------------------------------ */
    /*
     * 分类
     */

    function category($page = 1, $limit = 30) {
        if ($this->requestType === 'post') {
            $condition = [];
            $order = '';
            $field = '';
            $res = $this->service->model('NewsCategory')->getPageList($page, $limit, $condition, $order, $field);
            //dump($res);
            return json(['code' => 0, 'count' => $res['total_count'], 'data' => $res['data']]);
        }
        $this->assign('addButton', $this->html_template->addButton('添加分类', 'admin', 'news', 'addCategory'));
        $this->assign('batheDeleteButton', $this->html_template->batheDeleteButton('table', 'admin', 'news', 'deletecategory'));
        return $this->fetch();
    }

    /* ------------------------------------------------------------------ */
    /*
     * 添加新闻分类
     */

    function addCategory() {
        if ($this->requestType === 'post') {
            $data = input('post.');
            if ($this->service->model('NewsCategory')->add($data, 'addnewscategory')) {
                return $this->success('添加成功！');
            }
            return $this->error($this->service->getError());
        }
        $this->assign('langs', $this->service->model('Lang')->getList(['status' => 1]));
        return $this->fetch();
    }

    /* ------------------------------------------------------------------ */
    /*
     * 编辑新闻分类
     */

    function editCategory($id = '') {
        $category = $this->service->model('NewsCategory')->getInfo(['id' => $id]);
        if ($this->requestType === 'post') {
            $data = input('post.');
             if($data['name']==$category['name']){
                unset($data['name']);
            } else if(empty($data['name'])){
                return $this->error('名称不能为空');
            }
            if ($this->service->model('NewsCategory')->edit($data, ['id' => $id], 'editnewscategory')) {
                return $this->success('修改成功！');
            }
            return $this->error($this->service->getError());
        }
        $this->assign('langs', $this->service->model('Lang')->getList(['status' => 1]));
        $this->assign('category', $category);
        return $this->fetch();
    }

    /* ------------------------------------------------------------------ */
    /*
     * 删除新闻分类
     */

    function deleteCategory($id = '') {
        if ($this->service->model('NewsCategory')->delete($id)) {
            return $this->success('删除成功！');
        }
        return $this->error('删除失败！');
    }

    /* ------------------------------------------------------------------ */
}
