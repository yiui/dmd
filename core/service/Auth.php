<?php
namespace core\service;
// +----------------------------------------------------------------------
// | 系统配置管理操作
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2017 Even Yin All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Even yin <416467069@qq.com> <13022156261>
// +----------------------------------------------------------------------
use \think\Session as Session;          //用户Session缓存
use \think\Cache   as Cache;            //全局缓存
use core\model\AuthRuleModel as AuthRuleModel;
use core\model\AuthGroupModel as AuthGroupModel;
class Auth extends BaseService
{
    protected $rule;
    protected $group;
    /* -----------------------------------START------------------------------- */
    /*
    * 构造函数
    */
    public function __construct(){
      parent::__construct();
      $this->rule    = new AuthRuleModel();
      $this->group    = new AuthGroupModel();
    }
    /* -----------------------------------END------------------------------- */
    /*
    * 获取角色列表
    */
    public function getGroups($page=1,$page_size=30,$condition=[],$order='',$field=''){
      return $this->group->pageQuery($page, $page_size, $condition, 'id DESC', $field);
    }
    /* -----------------------------------END------------------------------- */
    /*
    * 添加角色
    */
    public function addGroup($data=[]){
      $user = new UserService();
      if($this->group->save($data)){
        $this->setLog($user->getSessionUid(),1,'添加角色SUCCESS');
        return true;
      }
      $this->error = $this->group->getError();
      $this->setLog($user->getSessionUid(),0,'添加角色ERROR:'.$this->error);
      return false;
    }
    /* -----------------------------------END------------------------------- */
    /*
    * 编辑角色
    */
    public function editGroup($id='',$data=[]){
      $user = new UserService();
      if($this->group->save($data,['id'=>$id])){
        $this->setLog($user->getSessionUid(),1,'编辑角色SUCCESS');
        return true;
      }
      $this->error = $this->group->getError();
      $this->setLog($user->getSessionUid(),0,'编辑角色ERROR:'.$this->error);
      return false;
    }
    /* -----------------------------------END------------------------------- */
    /*
    * 删除角色
    */
    public function deleteGroup($id=''){
      $user = new UserService();
      if($this->group->destroy($id)){
        $this->setLog($user->getSessionUid(),1,'删除规则SUCCESS');
        return true;
      }
      $this->error = $this->group->getError();
      $this->setLog($user->getSessionUid(),0,'删除规则ERROR:'.$this->error);
      return false;
    }
    /* -----------------------------------END------------------------------- */
    /*
    * 获取全部规则列表
    */
    public function getAllRules($condition, $field, $order){
      return $this->rule->getQuery($condition, $field, $order);
    }
    /* -----------------------------------END------------------------------- */
    /*
    * 获取规则列表
    */
    public function getRules($page=1,$page_size=30,$condition=[],$order='',$field=''){
      return $this->rule->pageQuery($page, $page_size, $condition, 'id DESC', $field);
    }
    /* -----------------------------------END------------------------------- */
    /*
    * 添加规则
    */
    public function addRule($data=[]){
      $user = new UserService();
      if($this->rule->save($data)){
        $this->setLog($user->getSessionUid(),1,'添加规则SUCCESS');
        return true;
      }
      $this->error = $this->rule->getError();
      $this->setLog($user->getSessionUid(),0,'添加规则ERROR:'.$this->error);
      return false;
    }
    /* -----------------------------------END------------------------------- */
    /*
    * 编辑规则
    */
    public function editRule($id='',$data=[]){
      $user = new UserService();
      if($this->rule->save($data,['id'=>$id])){
        $this->setLog($user->getSessionUid(),1,'编辑规则SUCCESS');
        return true;
      }
      $this->error = $this->rule->getError();
      $this->setLog($user->getSessionUid(),0,'编辑规则ERROR:'.$this->error);
      return false;
    }
    /* -----------------------------------END------------------------------- */
    /*
    * 删除规则
    */
    public function deleteRule($id=''){
      $user = new UserService();
      if($this->rule->destroy($id)){
        $this->setLog($user->getSessionUid(),1,'删除规则SUCCESS');
        return true;
      }
      $this->error = $this->rule->getError();
      $this->setLog($user->getSessionUid(),0,'删除规则ERROR:'.$this->error);
      return false;
    }

}
