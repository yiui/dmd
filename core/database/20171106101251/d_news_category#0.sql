DROP TABLE IF EXISTS `d_news_category`;
CREATE TABLE `d_news_category` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `lang` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='新闻分类';
insert into `d_news_category` VALUES  ( '2','热点新闻','0','1' ), ( '3','互联网','1','1' ), ( '5','国际视野','1','1' ) ;
