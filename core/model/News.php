<?php
namespace core\model;
// +----------------------------------------------------------------------
// | 新闻
// +----------------------------------------------------------------------
class News extends BaseModel
{
    protected $table      = 'news';
    protected $rule       = [];
    protected $scene_rule = [
        'title'=>'unique:News|max:50',
        'cover'=>'require',
        'description'=>'require',
        'content'=>'require',
    ];
    protected $msg        = [
        //'title.require'=>'标题必须',
        'title.unique'=>'标题必须唯一',
        'cover.require'=>'图片必须',
        'description'=>'描述必须',
        'content.require'=>'内容必须'
    ];
    protected $scene      = [
        'addnews'=>['title','cover','description','content'],
        'editnews'=>['title','cover','description','cotent'],
        
    ];
    protected function getLangAttr($value){
        $res=db('lang')->where('id',$value)->find();
       if($res){
           return $res['title'];
       }
       return null;
    }
    
    protected function getCidAttr($value){
        $res=db('news_category')->where('id',$value)->find();
       if($res){
           return $res['name'];
       }
       return null;
    }
}
