<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:65:"D:\wamp\www\dmd\public/../application/admin\view\index\index.html";i:1509352395;s:60:"D:\wamp\www\dmd\public/../application/admin\view\layout.html";i:1511749434;}*/ ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width,initial-scale=1, maximum-scale=1, user-scalable=no">
  <title>管理后台</title>
  <link rel="stylesheet" href="__PUBLIC__/layui/css/layui.css">
  <script src="__PUBLIC__/layui/layui.js"></script>
  <style>
    body{background-color: #eee;min-width: 1200px;}
    .container{margin:0 auto;width: 1200px;}
    .head{height: 80px;background-color: #fff;border-bottom: 1px solid #ddd;border-top:5px solid #444;}
    .head .logo{#float:left;background-image: url(#http://dmd.qianbeinet.com/dmd_logo.png);background-size: cover;width: 200px;height:40px;margin-top: 20px;float: left;}
    .head .layui-nav{background-color:#fff;border-radius:0;float: right;padding:18px 0;}
    .head .layui-nav .layui-nav-more{display: none}
    .head .layui-nav-child{top:62px;box-shadow:none;}
    .head .layui-nav .layui-nav-item{line-height: 24px;text-align: center;}
    .head .layui-nav .layui-nav-item p{font-size: 12px;}
    .head .layui-nav .layui-nav-item a{color: #7A7A7A;font-size: 13px;padding:10px 36px;}
    .head .layui-nav .layui-nav-item a:hover,.head .layui-nav .layui-this a{color: #fff;background-color: #444;font-size: 13px;}
    .head .layui-nav .layui-this:after, .layui-nav-bar, .layui-nav-tree .layui-nav-itemed:after{height:0;}

    .footer{background-color: #eee;color:rgba(0, 0, 0, 0.5);font:14px Helvetica Neue,Helvetica,PingFang SC,\5FAE\8F6F\96C5\9ED1,Tahoma,Arial,sans-serif;}
    .footer a{color:rgba(255,255,255,.6);line-height: 24px;font-size: 13px;color:rgba(255,255,255,.3);}
    .footer .footer-link-block{min-height: 200px;}
    .footer .footer-link-title{font-size: 16px;color: rgba(255,255,255,.8);margin-bottom: 10px;}

    .layui-btn{background-color: #444;}
    .layui-btn-normal{background-color: #1E9FFF;}
    .layui-btn-danger{background-color: #FF5722;}
    .layui-btn-primary{background-color: #fff;}
    .layui-btn-primary:hover{border-color:#444;}
  </style>
  <style media="screen">
    .teacher{min-height:600px;background-color:#fff;border:1px solid #ddd;}
    .layui-nav-tree .layui-nav-item a:hover{}
      .layui-nav-tree .layui-nav-bar{width: 0}
    .layui-nav-tree .layui-nav-item{line-height: 48px}
    .layui-nav-tree .layui-nav-item a{height: 48px;color: #000;text-align: center;}
    .layui-nav-tree .layui-nav-item a:hover{background-color:#444;}
    .layui-nav-tree.layui-nav{background-color: #fff}
    .layui-nav-tree .layui-nav-child dd.layui-this,
     .layui-nav-tree .layui-nav-child dd.layui-this a,
      .layui-nav-tree .layui-this,
       .layui-nav-tree .layui-this>a,
       .layui-nav-tree .layui-this>a:hover{background-color: #444;}
    .layui-nav-tree .layui-this>a{color: #fff;}

    .teacher .layui-tab-card{border: 0;box-shadow: none;border-radius: 0}
    .teacher .layui-tab{margin: 0}
    .teacher .layui-tab .layui-tab-title{height: 48px;background-color: #fff;}
    .teacher .layui-tab .layui-tab-title li{line-height: 48px;padding:0 65px;}
    .teacher .layui-tab .layui-tab-title li.layui-this a{color: #fff;}
    .teacher .layui-tab .layui-tab-title .layui-this{background-color: #444;color: #fff}
    .teacher .layui-tab .layui-tab-title .layui-this:after{height: 48px;border: 0;}
    .teacher .layui-tab .layui-tab-content{padding: 20px}
    .teacher .layui-tab .layui-tab-title .layui-tab-bar{height: 48px;line-height: 48px;}

    .layui-form-item{margin-bottom: 30px;}
    input:disabled{background-color: #eee}
    .layui-form-select dl dd.layui-this{background-color: #444;}
    .layui-laydate .layui-this{background-color: #444!important;}
    .layui-laypage .layui-laypage-curr .layui-laypage-em{background-color: #444;}
  </style>
  
</head>

<body>
  
    <header class="">
      <div class="head">
        <div class="container">
          <div class="layui-row">
            <div class="layui-col-xs2">
              <a href="<?php echo url('index/index'); ?>" class="logo"></a>
            </div>
            <div class="layui-col-xs10">
              <?php if(!empty($user)): ?>
              <ul class="layui-nav">
<!--                <li class="layui-nav-item"><a href="/" target="_blank">官网首页</a></li>-->
                <li class="layui-nav-item <?php if(in_array($controller,['index','system'])): ?>layui-this<?php endif; ?>"><a href="<?php echo url('index/index'); ?>">系统管理</a></li>
                <li class="layui-nav-item <?php if(in_array($controller,['news'])): ?>layui-this<?php endif; ?>"><a href="<?php echo url('news/index'); ?>">内容管理</a></li>
                <li class="layui-nav-item <?php if(in_array($controller,['spec'])): ?>layui-this<?php endif; ?>"><a href="<?php echo url('spec/index'); ?>">服务公司管理</a></li>
                <li class="layui-nav-item <?php if(in_array($controller,['user'])): ?>layui-this<?php endif; ?>">
                  <a href="javascript:;">用户中心</a>
                  <dl class="layui-nav-child">
                    <dd><a href="<?php echo url('user/index'); ?>" style="background-color:#fff;color:#7A7A7A">个人信息</a></dd>
                    <dd><a href="<?php echo url('index/clearCache'); ?>" style="background-color:#fff;color:#7A7A7A">更新缓存</a></dd>
                    <dd><a href="<?php echo url('user/logout'); ?>" style="background-color:#fff;color:#7A7A7A">安全退出</a></dd>
                  </dl>
                </li>
                </ul>
              <?php endif; ?>
            </div>
          </div>
        </div>
      </div>
    </header>
  
  
  <div class="container">
    <div class="layui-row layui-col-space10" style="margin-top:5px;">
      <div class="layui-col-xs2">
        <div class="teacher">
          
  <ul class="layui-nav layui-nav-tree" style="width:100%;">
    <li class="layui-nav-item <?php if(in_array(request()->action(),['index'])): ?>layui-this<?php endif; ?>"><a href="<?php echo url('index/index'); ?>">主页</a></li>
    <li class="layui-nav-item <?php if(in_array(request()->action(),['menu'])): ?>layui-this<?php endif; ?>"><a href="<?php echo url('system/menu'); ?>">菜单管理</a></li>
    <li class="layui-nav-item <?php if(in_array(request()->action(),['wechat'])): ?>layui-this<?php endif; ?>"><a href="<?php echo url('system/wechat'); ?>">公众号管理</a></li>
    <li class="layui-nav-item <?php if(in_array(request()->action(),['users'])): ?>layui-this<?php endif; ?>"><a href="<?php echo url('system/users'); ?>">用户管理</a></li>
    <li class="layui-nav-item <?php if(in_array(request()->action(),['ad'])): ?>layui-this<?php endif; ?>"><a href="<?php echo url('system/ad'); ?>">广告图管理</a></li>
    <li class="layui-nav-item <?php if(in_array(request()->action(),['lang'])): ?>layui-this<?php endif; ?>"><a href="<?php echo url('system/lang'); ?>">多语言管理</a></li>
    <li class="layui-nav-item <?php if(in_array(request()->action(),['databases'])): ?>layui-this<?php endif; ?>"><a href="<?php echo url('system/databases'); ?>">数据库管理</a></li>
    <li class="layui-nav-item <?php if(in_array(request()->action(),['conf'])): ?>layui-this<?php endif; ?>"><a href="<?php echo url('system/conf'); ?>">系统配置</a></li>
  </ul>

        </div>
      </div>
      <div class="layui-col-xs10">
        <div class="teacher">
          
  <div class="layui-tab layui-tab-card">
    <ul class="layui-tab-title">
      <li class="layui-this">公司信息</li>
      <li>软件信息</li>
      <li>服务器信息</li>
      <li>数据库信息</li>
      <li>PHP相关参数</li>
    </ul>
    <div class="layui-tab-content">
      <div class="layui-tab-item layui-show">
        <table class="layui-table">
            <tr>
                <td>当前语言版本</td>
                <td><?php echo !empty($system['title'])?$system['title']:'中文'; ?></td>
            </tr>
            <tr>
                <td width="40%">网站名称</td>
                <td width="60%"><?php echo $system['web_name']; ?></td>
            </tr>
            <tr>
                <td>公司名称</td>
                <td><?php echo $system['company_name']; ?></td>
            </tr>
            <tr>
                <td>公司地址</td>
                <td><?php echo $system['addr']; ?></td>
            </tr>
            <tr>
                <td>公司联系人</td>
                <td><?php echo !empty($system['contacts'])?$system['contacts']:''; ?></td>
            </tr>
            <tr>
                <td>公司电话</td>
                <td><?php echo $system['mobile']; ?></td>
            </tr>
            <tr>
                <td>公司邮箱</td>
                <td><?php echo $system['email']; ?></td>
            </tr>
            <tr>
                <td>公司介绍</td>
                <td><?php echo $system['introduce']; ?></td>
            </tr>
        </table>
      </div>
      <div class="layui-tab-item">
        <table class="layui-table">
            <tr>
                <td width="40%">软件名称</td>
                <td width="60%">多用户视频网站平台</td>
            </tr>
            <tr>
                <td>系统版本</td>
                <td>v2.0.0</td>
            </tr>
            <tr>
                <td>QQ</td>
                <td>416467069 或者点击 <a href="tencent://AddContact/?fromId=45&fromSubId=1&subcmd=all&uin=416467069&website=<?php echo $_SERVER["HTTP_HOST"]; ?>">添加</a></td>
            </tr>
            <tr>
                <td>官网</td>
                <td><a href="">Even</a></td>
            </tr>
        </table>
      </div>
      <div class="layui-tab-item">
        <table class="layui-table">
            <tr>
                <td width="40%">服务器域名</td>
                <td width="60%"><?php echo $_SERVER["HTTP_HOST"]; ?></td>
            </tr>
            <tr>
                <td>服务器标识</td>
                <td><?php echo $_SERVER["SERVER_SOFTWARE"]; ?></td>
            </tr>
            <tr>
                <td>服务器操作系统</td>
                <td><?php echo php_uname('s'); ?></td>
            </tr>
            <tr>
                <td>服务器语言</td>
                <td>PHP-<?php echo PHP_VERSION; ?></td>
            </tr>
            <tr>
                <td>服务器端口</td>
                <td><?php echo $_SERVER['SERVER_PORT']; ?></td>
            </tr>
            <tr>
                <td>服务器主机名</td>
                <td><?php echo GetHostByName($_SERVER['SERVER_NAME']); ?></td>
            </tr>
            <tr>
                <td>目录物理路径</td>
                <td><?php echo ROOT_PATH; ?></td>
            </tr>
        </table>
      </div>
      <div class="layui-tab-item">
        <table class="layui-table">
            <tr>
                <td width="40%">数据库版本</td>
                <td width="60%"><?php echo $db_version; ?></td>
            </tr>
            <tr>
                <td>数据库名称</td>
                <td><?php echo config('database.database'); ?></td>
            </tr>
            <tr>
                <td>数据库大小</td>
                <td><?php echo $db_size; ?></td>
            </tr>
        </table>
      </div>
      <div class="layui-tab-item">
        <table class="layui-table">
            <tr>
                <td width="40%">PHP版本</td>
                <td width="60%"><?php echo PHP_VERSION; ?></td>
            </tr>
            <tr>
                <td>上传文件最大限制</td>
                <td><?php echo ini_get("file_uploads") ? ini_get("upload_max_filesize") : "Disabled"; ?></td>
            </tr>
            <tr>
                <td>脚本运行占用最大内存</td>
                <td><?php echo get_cfg_var ("memory_limit")?get_cfg_var("memory_limit"):"无"; ?></td>
            </tr>
            <tr>
                <td>最大执行时间</td>
                <td><?php echo ini_get("max_execution_time")."秒"; ?></td>
            </tr>

            <tr>
                <td>POST提交最大限制</td>
                <td><?php echo ini_get("post_max_size")?:"Disabled"; ?></td>
            </tr>
        </table>
      </div>
    </div>
  </div>

        </div>
      </div>
    </div>
  </div>
  
    <footer class="footer">
      <div class="container">
        <div style="padding:30px 0;">
          <div style="clear:both;text-align:center;">Copyright © 2017 <?php echo request()->domain(); ?> 版权所有</div>
        </div>
      </div>
    </footer>
  
  <script>
      layui.use(['element'],function(){
        var element = layui.element;
      });
  </script>
  
</body>
</html>
