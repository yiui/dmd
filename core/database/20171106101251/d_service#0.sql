DROP TABLE IF EXISTS `d_service`;
CREATE TABLE `d_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(22) NOT NULL DEFAULT ' ',
  `cover` varchar(100) DEFAULT NULL,
  `description` text NOT NULL,
  `content` text NOT NULL,
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `lang` int(1) DEFAULT '1',
  `status` int(1) DEFAULT NULL,
  `is_public` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='服务项目表';
insert into `d_service` VALUES  ( '2','3333','\uploads\1\20171104\2ca33fa197cf5803b116ad5e487311be.jpg','333','3333','1509764584','1509764584','','1','1','' ), ( '3','44444444444','\uploads\1\20171104\f25a77767c087cca00c2b71ca0e0a939.jpg','44444444444','444444444444444444','1509764679','1509764679','','1','1','' ), ( '4','555','\uploads\1\20171104\099ad9e3e2f60bb8c11bfd1cf6adbb43.jpg','555555555222222','55555555555','1509764883','1509764966','','1','1','' ), ( '5','5555','\uploads\1\20171106\ebb2095063379353ba9e9d119fec4736.jpg','财经一习谈| 这四年，精准扶贫实现历史跨越','<h1>财经一习谈| 这四年，精准扶贫实现历史跨越</h1>','1509931639','1509932564','','1','1','' ) ;
