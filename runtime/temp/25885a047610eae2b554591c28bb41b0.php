<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:70:"D:\wamp\www\dmd\public/../application/admin\view\system\databases.html";i:1509519006;s:60:"D:\wamp\www\dmd\public/../application/admin\view\layout.html";i:1509011174;}*/ ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width,initial-scale=1, maximum-scale=1, user-scalable=no">
  <title>管理后台|江苏大名大律师事务所</title>
  <link rel="stylesheet" href="__PUBLIC__/layui/css/layui.css">
  <script src="__PUBLIC__/layui/layui.js"></script>
  <style>
    body{background-color: #eee;min-width: 1200px;}
    .container{margin:0 auto;width: 1200px;}
    .head{height: 80px;background-color: #fff;border-bottom: 1px solid #ddd;border-top:5px solid #444;}
    .head .logo{#float:left;background-image: url(http://dmd.qianbeinet.com/dmd_logo.png);background-size: cover;width: 200px;height:40px;margin-top: 20px;float: left;}
    .head .layui-nav{background-color:#fff;border-radius:0;float: right;padding:18px 0;}
    .head .layui-nav .layui-nav-more{display: none}
    .head .layui-nav-child{top:62px;box-shadow:none;}
    .head .layui-nav .layui-nav-item{line-height: 24px;text-align: center;}
    .head .layui-nav .layui-nav-item p{font-size: 12px;}
    .head .layui-nav .layui-nav-item a{color: #7A7A7A;font-size: 13px;padding:10px 36px;}
    .head .layui-nav .layui-nav-item a:hover,.head .layui-nav .layui-this a{color: #fff;background-color: #444;font-size: 13px;}
    .head .layui-nav .layui-this:after, .layui-nav-bar, .layui-nav-tree .layui-nav-itemed:after{height:0;}

    .footer{background-color: #eee;color:rgba(0, 0, 0, 0.5);font:14px Helvetica Neue,Helvetica,PingFang SC,\5FAE\8F6F\96C5\9ED1,Tahoma,Arial,sans-serif;}
    .footer a{color:rgba(255,255,255,.6);line-height: 24px;font-size: 13px;color:rgba(255,255,255,.3);}
    .footer .footer-link-block{min-height: 200px;}
    .footer .footer-link-title{font-size: 16px;color: rgba(255,255,255,.8);margin-bottom: 10px;}

    .layui-btn{background-color: #444;}
    .layui-btn-normal{background-color: #1E9FFF;}
    .layui-btn-danger{background-color: #FF5722;}
    .layui-btn-primary{background-color: #fff;}
    .layui-btn-primary:hover{border-color:#444;}
  </style>
  <style media="screen">
    .teacher{min-height:600px;background-color:#fff;border:1px solid #ddd;}
    .layui-nav-tree .layui-nav-item a:hover{}
      .layui-nav-tree .layui-nav-bar{width: 0}
    .layui-nav-tree .layui-nav-item{line-height: 48px}
    .layui-nav-tree .layui-nav-item a{height: 48px;color: #000;text-align: center;}
    .layui-nav-tree .layui-nav-item a:hover{background-color:#444;}
    .layui-nav-tree.layui-nav{background-color: #fff}
    .layui-nav-tree .layui-nav-child dd.layui-this,
     .layui-nav-tree .layui-nav-child dd.layui-this a,
      .layui-nav-tree .layui-this,
       .layui-nav-tree .layui-this>a,
       .layui-nav-tree .layui-this>a:hover{background-color: #444;}
    .layui-nav-tree .layui-this>a{color: #fff;}

    .teacher .layui-tab-card{border: 0;box-shadow: none;border-radius: 0}
    .teacher .layui-tab{margin: 0}
    .teacher .layui-tab .layui-tab-title{height: 48px;background-color: #fff;}
    .teacher .layui-tab .layui-tab-title li{line-height: 48px;padding:0 65px;}
    .teacher .layui-tab .layui-tab-title li.layui-this a{color: #fff;}
    .teacher .layui-tab .layui-tab-title .layui-this{background-color: #444;color: #fff}
    .teacher .layui-tab .layui-tab-title .layui-this:after{height: 48px;border: 0;}
    .teacher .layui-tab .layui-tab-content{padding: 20px}
    .teacher .layui-tab .layui-tab-title .layui-tab-bar{height: 48px;line-height: 48px;}

    .layui-form-item{margin-bottom: 30px;}
    input:disabled{background-color: #eee}
    .layui-form-select dl dd.layui-this{background-color: #444;}
    .layui-laydate .layui-this{background-color: #444!important;}
    .layui-laypage .layui-laypage-curr .layui-laypage-em{background-color: #444;}
  </style>
  
</head>

<body>
  
    <header class="">
      <div class="head">
        <div class="container">
          <div class="layui-row">
            <div class="layui-col-xs2">
              <a href="<?php echo url('index/index'); ?>" class="logo"></a>
            </div>
            <div class="layui-col-xs10">
              <?php if(!empty($user)): ?>
              <ul class="layui-nav">
                <li class="layui-nav-item"><a href="/" target="_blank">官网首页</a></li>
                <li class="layui-nav-item <?php if(in_array($controller,['index','system'])): ?>layui-this<?php endif; ?>"><a href="<?php echo url('index/index'); ?>">系统管理</a></li>
                <li class="layui-nav-item <?php if(in_array($controller,['news'])): ?>layui-this<?php endif; ?>"><a href="<?php echo url('news/index'); ?>">内容管理</a></li>
                <li class="layui-nav-item <?php if(in_array($controller,['spec'])): ?>layui-this<?php endif; ?>"><a href="<?php echo url('spec/index'); ?>">服务公司管理</a></li>
                <li class="layui-nav-item <?php if(in_array($controller,['user'])): ?>layui-this<?php endif; ?>">
                  <a href="javascript:;">用户中心</a>
                  <dl class="layui-nav-child">
                    <dd><a href="<?php echo url('user/index'); ?>" style="background-color:#fff;color:#7A7A7A">个人信息</a></dd>
                    <dd><a href="<?php echo url('index/clearCache'); ?>" style="background-color:#fff;color:#7A7A7A">更新缓存</a></dd>
                    <dd><a href="<?php echo url('user/logout'); ?>" style="background-color:#fff;color:#7A7A7A">安全退出</a></dd>
                  </dl>
                </li>
                </ul>
              <?php endif; ?>
            </div>
          </div>
        </div>
      </div>
    </header>
  
  
  <div class="container">
    <div class="layui-row layui-col-space10" style="margin-top:5px;">
      <div class="layui-col-xs2">
        <div class="teacher">
          
  <ul class="layui-nav layui-nav-tree" style="width:100%;">
    <li class="layui-nav-item <?php if(in_array(request()->action(),['index'])): ?>layui-this<?php endif; ?>"><a href="<?php echo url('index/index'); ?>">主页</a></li>
    <li class="layui-nav-item <?php if(in_array(request()->action(),['menu'])): ?>layui-this<?php endif; ?>"><a href="<?php echo url('system/menu'); ?>">菜单管理</a></li>
    <li class="layui-nav-item <?php if(in_array(request()->action(),['wechat'])): ?>layui-this<?php endif; ?>"><a href="<?php echo url('system/wechat'); ?>">公众号管理</a></li>
    <li class="layui-nav-item <?php if(in_array(request()->action(),['users'])): ?>layui-this<?php endif; ?>"><a href="<?php echo url('system/users'); ?>">用户管理</a></li>
    <li class="layui-nav-item <?php if(in_array(request()->action(),['ad'])): ?>layui-this<?php endif; ?>"><a href="<?php echo url('system/ad'); ?>">广告图管理</a></li>
    <li class="layui-nav-item <?php if(in_array(request()->action(),['lang'])): ?>layui-this<?php endif; ?>"><a href="<?php echo url('system/lang'); ?>">多语言管理</a></li>
    <li class="layui-nav-item <?php if(in_array(request()->action(),['databases'])): ?>layui-this<?php endif; ?>"><a href="<?php echo url('system/databases'); ?>">数据库管理</a></li>
    <li class="layui-nav-item <?php if(in_array(request()->action(),['conf'])): ?>layui-this<?php endif; ?>"><a href="<?php echo url('system/conf'); ?>">系统配置</a></li>
  </ul>

        </div>
      </div>
      <div class="layui-col-xs10">
        <div class="teacher">
          

<div class="layui-tab layui-tab-brief" style="margin:0;box-shadow:none;border:0;">
  <ul class="layui-tab-title">
    <li class="layui-this">数据库备份</li>
  </ul>
  <div class="layui-tab-content" style="padding:20px;background:#fff;">
    <fieldset class="layui-elem-field site-demo-button" style="padding:20px;margin-bottom:20px;">
      <legend>操作</legend>
      <div>
        <button class="layui-btn layui-btn-primary" id="backup">备份数据</button>
        <button class="layui-btn layui-btn-primary" id="batchDelete">批量删除</button>
      </div>
    </fieldset>


    <table class="layui-table" lay-data="{skin: 'line',method:'post', url:'<?php echo url('system/databases',input('get.')); ?>', page:true, id:'table'}" lay-filter="table">
      <thead>
        <tr>
          <th lay-data="{checkbox:true, fixed: true}"></th>
          <th lay-data="{field:'name', align:'center', width:200}">Name</th>
          <th lay-data="{field:'size', align:'center', width:200}">Size</th>
          <th lay-data="{field:'time', align:'center', width:200}">Time</th>
          <th lay-data="{width:306, align:'center', toolbar: '#barDemo'}">操作</th>
        </tr>
      </thead>
    </table>
    <script type="text/html" id="barDemo">
      <a class="layui-btn layui-btn-warm layui-btn-mini" lay-event="download">下载</a>
      <a class="layui-btn layui-btn-normal layui-btn-mini" lay-event="recovery">恢复</a>
      <a class="layui-btn layui-btn-danger layui-btn-mini" lay-event="delete">删除</a>
    </script>
    <script>
      layui.use(['table','jquery'], function(){
        var table = layui.table;
        var $ = layui.jquery;

        $('#backup').on('click',function(){
          layer.open({
            type: 1
            ,title:'数据库备份'
            ,content: '<div style="padding: 50px 100px;">请不要关闭本弹窗！系统会自动关闭，数据正在备份中...<i style="font-size:16px;" class="layui-icon layui-anim layui-anim-rotate layui-anim-loop">&#xe63d;</i></div>'
            ,btn: ''
            ,btnAlign: 'r' //按钮居中
            ,shade: .6 //不显示遮罩
          });
          $.post('<?php echo url('system/backupDatabase'); ?>',{},function(res){
            if(res.code==1){
              layer.closeAll();
              location.href = location;
            }else{
              layer.msg(res.msg,{icon:5,anim:6});
            }
          })
        })

        $('#batchDelete').on('click',function(){
          var checkStatus = table.checkStatus('table'),data = checkStatus.data;
          if(data.length<1){
              layer.msg('未选中要删除的数据！',{icon:5,anim:6});
              return false;
          }
          var name = new Array();
          for(var i=0;i<data.length;i++){
            name.push(data[i].name);
          }
          layer.alert();
          name = name.toString();
          layer.confirm('确定要删除ID：'+name, function(index){
            $.post('<?php echo url('system/deleteDatabase'); ?>',{name:name},function(res){
              layer.close(index);
              if(res.code==1){
                location.href = location;
              }else{
                layer.msg(res.msg);
              }
            })
          });
        })
        table.on('tool(table)', function(obj){
          var data = obj.data;
          if(obj.event === 'delete'){
            layer.confirm('确认真的删除吗？', function(index){
              $.post('<?php echo url('system/deleteDatabase'); ?>',{name:data.name},function(res){
                if(res.code==1){
                  obj.del();
                  layer.close(index);
                }else{
                  layer.msg(res.msg);
                  layer.close(index);
                }
              })
            });
          }else if(obj.event === 'recovery'){
            layer.confirm('确认真的恢复到此数据库版本吗？', function(index){
              $.post('<?php echo url('system/recoveryDatabase'); ?>',{name:data.name},function(res){
                layer.msg(res.msg);
                layer.close(index);
              })
            });
          }else if(obj.event === 'download'){
            location.href = '<?php echo url('system/downloadDatabase'); ?>?name='+data.name;
          }
        });
      });
    </script>


  </div>

</div>

        </div>
      </div>
    </div>
  </div>
  
    <footer class="footer">
      <div class="container">
        <div style="padding:30px 0;">
          <div style="clear:both;text-align:center;">Copyright © 2017 <?php echo request()->domain(); ?> 版权所有</div>
        </div>
      </div>
    </footer>
  
  <script>
      layui.use(['element'],function(){
        var element = layui.element;
      });
  </script>
  
  <script>
    layui.use(['form', 'layedit', 'laydate'], function(){
      var form = layui.form
      ,layer = layui.layer
      ,laydate = layui.laydate;
      //日期
      laydate.render({
        elem: '#date'
      });
    });
  </script>

</body>
</html>
